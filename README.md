# 简单的门户网站及管理端

#### 介绍
基于PortalSite1项目修改，原项目地址https://gitee.com/wanglonglongjm/PortalSite1<br>
升级了框架版本，门户网站页面修改较少，<br>
对后台管理部分的java代码几乎全部重写，后台管理页面使用freemarker生成<br>
使用shiro增强后台管理安全性和可配置性<br>
项目经过简单测试，可能存在其他测试未覆盖到的问题未处理<br>
[项目演示地址](https://www.wenruitao.top/sps)

#### 软件架构
核心框架：springboot 2.5.2

数据层: spring data jpa,hibernate

项目构建工具：maven

Web容器：tomcat

前端框架：layui

数据交互：ajax

数据库推荐使用MySQL 5.7及以上版本，<br>
项目在第一次运行时，会自动创建相应的表格<br>
请注意查阅服务实现层中，使用了@PostConstruct注解的部分，<br>
项目运行启动时，会自动检查权限、角色和管理员账号是否存在，<br>
若不存在，将自动创建；<br>
若已存在，将跳过（不进行任何操作）。

#### 系统结构
1. 前端页面

- 门户网站<br>
静态html页面，放置在src\main\resources\static\html路径下<br>
注：静态资源，存放在src\main\resources\static\路径
注：虽说是静态页面，但不可脱离项目单独使用，因为jq和layui是通过webjar引入，项目没有启动时无法正常载入
- 管理端页面<br>
使用freemarker模板生成（后端生成）页面,模板文件放置在src\main\resources\templates路径下

2. 后端代码

主要区分为后台管理部分(路径包含‘mgt’)和门户网站部分(路径包含‘web’)
- *.entity 领域实体类
- *.pojo 数据实体类
- *.dao 数据层
- *.service 服务层
- *.controller 接口层<br>
代码均有javadoc注释

#### 安装教程

1. 项目可通过maven打包成一个war，放入tomcat运行
1. 若导入开发工具eclipse或idea，可直接运行src\main\java\com\wrt\sps\SimplePortalSiteApplication.java启动项目
1. 打包或运行前，请注意修改application.yml中的配置
1. 项目运行前，请创建数据库sps（可以改为其他名称，请注意修改yml配置文件），系统在运行后会自动创建数据库表并录入管理员信息


#### 使用说明

1. 项目默认使用8082端口
1. 通过/html接口访问网站，如http://localhost:8082/html
1. 网站采用了响应式布局，目前移动端显示效果还在优化中
1. 通过/mgt接口访问网站管理端，如http://localhost:8082/mgt/index
1. 管理端默认用户名admin，密码123456
1. 每日23点清理未使用的文件（目前清理时间写死在代码里，后续增加上传文件管理和任务配置）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
