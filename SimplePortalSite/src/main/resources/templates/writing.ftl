<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>${titleName}管理</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js"></script>
    <script type="application/javascript" src="/js/mask.js"></script>
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	fieldset{
    		width: 95%;
    		margin: 2px auto;
    	}
    </style>
</head>
<body>
<fieldset class="layui-elem-field">
    <legend>查询条件</legend>
    <form id="form1" class="layui-form" action="">
    	<input name="type" type="hidden" value="${typeName}">
        <div class="layui-form-item">
        	<div class="layui-inline">
        		<label class="layui-form-label">${titleName}标题</label>
			    <div class="layui-input-block">
			    	<input id="title" name="title" class="layui-input" type="text" placeholder="请输入标题"
	                           autocomplete="off">
			    </div>
        	</div>
        	<div class="layui-inline">
        		<label class="layui-form-label">创建时间</label>
			    <div class="layui-input-block">
			    	<input id="createdDate" class="layui-input" type="text" placeholder="时间范围"
                           autocomplete="off">
                    <input name="starttime" type="hidden" value="">
                    <input name="endtime" type="hidden" value="">
			    </div>
        	</div>
        	<div class="layui-inline">
        		<label class="layui-form-label">显示状态</label>
			    <div class="layui-input-block">
			    	<input name="validFlag" type="radio" lay-skin="switch" value=""
                           title="所有" checked>
			    	<input name="validFlag" type="radio" lay-skin="switch" value="true"
                           title="展示中">
                    <input name="validFlag" type="radio" lay-skin="switch" value="false"
                           title="隐藏">
			    </div>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button class="layui-btn" lay-submit lay-filter="query">查询</button>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
        	</div>
        </div>
    </form>
</fieldset>

<fieldset class="layui-elem-field">
    <legend>查询结果</legend>
    <div class="layui-inline" style="margin:3px 10px;">
    	<#if addPermission>
        	<button class="layui-btn" onclick="add()">新增${titleName}</button>
        </#if>
    </div>
    <table id="gridresult" class="layui-table layui-form" lay-filter="gridfilter" lay-data="{url:'/mgt/writing/query?type=${typeName}', page:true, id:'gridresult',height:'630px'}">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox', fixed: 'left', align:'center', hide:true}"></th>
            <th lay-data="{field:'id', width:'5%', hide:true}">${titleName}id</th>
            <th lay-data="{field:'title', width:'40%', align:'center'}">${titleName}标题</th>
            <th lay-data="{field:'createdDate', width:'20%', sort:true, align:'center'}">创建时间</th>
            <th lay-data="{field:'validFlag', width:'15%', templet:'#switchTpl', align:'center'}">显示状态</th>
            <th lay-data="{fixed:'right', align:'center', toolbar:'#barDemo'}">操作</th>
        </tr>
        </thead>
    </table>

</fieldset>
</body>
<script id="switchTpl" type="text/html">
    <input type="checkbox" name="validflag" value="{{ d.id }}" lay-skin="switch" lay-filter="toggleShow" lay-text="展示中|隐藏"
     {{ d.validFlag?'checked' : '' }} <#if !editPermission>disabled</#if>>
</script>
<script id="barDemo" type="text/html">
	<#if editPermission>
    	<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </#if>
    <#if delPermission>
    	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </#if>
    <#if !delPermission && !editPermission>
    	<span>无</span>
    </#if>
</script>
<script type="application/javascript">
    layui.use(['form', 'laydate', 'table'], function () {
        var form = layui.form;
        
        //switch off状态赋值
        /*form.on('switch(switchshow)', function (data) {
            $(data.elem).attr("type", "hidden").val(this.checked ? true : false);
        });*/
        <#if editPermission>
        form.on('switch(toggleShow)', function (data) {
            $.ajax({
	            type: "post",
	            url: "/mgt/writing/display",
	            data: {"id":data.value, "isShow":this.checked},
	            dataType: "json",
	            success: function (data) {
	            	if(data.code == 0){
	                	layer.msg('修改成功', {
		                	icon: 1,
	                		offset: [msgtop+'px', msgleft+'px']
	                		},
		                	function(){
	                    		window.location.reload();
		                	}
		                );
		             } else {
		             	layer.msg(data.msg, {
		            		icon: 2,
	                		offset: [msgtop+'px', msgleft+'px']
		            	});
		             }
	            },
	            error: function () {
	            	layer.msg("修改失败", {
	            		icon: 2,
	                	offset: [msgtop+'px', msgleft+'px']
	            	});
	            }
	        });
        });
        </#if>
        //监听提交
        form.on('submit(query)', function (data) {
            table.reload('gridresult', {
                url: '/mgt/writing/query'
                , where: data.field //设定异步数据接口的额外参数
                , page: {
                    page: 1
                }
            });
            return false;
        });

        //日期控件
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#createdDate', //指定元素
            range: true,
            done: function(value, date, endDate){
            	var start = dateFormat(date, true);
            	var end = dateFormat(endDate, false);
            	$("input[name='starttime']").val(start);
            	$("input[name='endtime']").val(end);
            }
        });
        

        //数据表格模块
        var table = layui.table;
        
        var msgtop = document.body.clientHeight / 2;
        var msgleft = document.body.clientWidth / 2;
        //监听工具条
        table.on('tool(gridfilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
            <#if delPermission>
                $.ajax({
                    type: "DELETE",
                    url: "/mgt/writing/delete",
                    data: {"id":data.id},
                    dataType: "json",
                    success: function (data) {
                    	if(data.code == 0){
	                    	layer.msg('删除成功', {
	                    			icon: 1,
	                    			offset: [msgtop+'px', msgleft+'px']
			                	},
			                	function(){
	                        		obj.del();
			                	}
			                );
			             } else {
			             	layer.msg(data.msg, {
			            		icon: 2,
	                    		offset: [msgtop+'px', msgleft+'px']
			            	});
			             }
                    },
                    error: function () {
                    	layer.msg("删除失败", {
		            		icon: 2,
	                    	offset: [msgtop+'px', msgleft+'px']
		            	});
                    }
                });
            </#if>
            } else if (obj.event === 'edit') {
            <#if editPermission>
                update(data.id);
            </#if>
            }
        });
    });
    
    function dateFormat(date, isStart){
    	if(!date){
    		return null;
    	}
    	var result = date.year + "-" + date.month + "-" + date.date;
    	if(isStart){
    		result += " 00:00:00";
    	} else {
    		result += " 23:59:59";
    	}
    	return result;
    }
    
    function toggleShow(id){
    	console.log(id);
    }
	<#if editPermission>
    function update(id) {
        if (!id || id == 0) {
            layer.alert("失败,请重新打开这个菜单");
            return;
        }
        var url = '/mgt/writing/edit?id=' + id + "&type=${typeName}";
        var name = '${titleName}'+"编辑";
        common(url, name);
    }
	</#if>
	<#if addPermission>
    function add() {
        var url = '/mgt/writing/add?type=${typeName}';
        var name = '${titleName}'+"新增";
        common(url, name);
    }
	</#if>
	function common(url, name) {
		var iWidth = 720;
        var iHeight = 600;
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
        window.open(url, name, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',menubar=0,scrollbars=1, resizable=1,status=1,titlebar=0,toolbar=0,location=1');
    }
</script>
</html>
