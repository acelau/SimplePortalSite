<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <title>后台管理员登录页面</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js"></script>
    <script type="application/javascript" src="/js/mask.js"></script>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css">
    <link rel="stylesheet" href="/css/mgt/login.css">
    <link rel="stylesheet" href="/css/load/load.css" media="all">
</head>
<body>
<div id="wrapper" class="login-page">
	<h1>${systemName}-${systemVersion}</h1>
    <div id="login_form" class="form">
        <form class="layui-form layui-form-pane">
        	<div class="layui-form-item">
    			<label class="layui-form-label">
    				<em class="layui-icon layui-icon-username"></em>
    			</label>
    			<div class="layui-input-block">
    				<input type="text" name="loginName" placeholder="用户名" lay-verify="required" autocomplete="off" class="layui-input"/>
		        </div>
            </div>
            <div class="layui-form-item">
    			<label class="layui-form-label">
    				<em class="layui-icon layui-icon-key"></em>
    			</label>
    			<div class="layui-input-block">
    				<input type="password" name="password" placeholder="密码" lay-verify="required" autocomplete="off" class="layui-input"/>
		        </div>
            </div>
            <button id="login" lay-submit lay-filter="*" class="layui-btn">登　录</button>
        </form>
    </div>
</div>
</body>
<script type="application/javascript">
	layui.use(['form', 'layer'], function(){
		var form = layui.form;
		var layer = layui.layer;
	  	form.on('submit(*)', function(data){
			$.ajax({
				url:'/mgt/login',
	            async:true,
	            data:data.field,
	            dataType:'json',
	            type:'POST',
	            success:function(xhrdata) {
	                if(0!=xhrdata.code) {
	                	layer.msg(xhrdata.msg, {
	                		icon: 0
	                	}, function(){
	                		shakeform();
	                	});
	                    return;
	                }
	                layer.msg('登录成功', 
	                	{icon: 1},
	                	function(){
	                		window.location.href = '/mgt/main';
	                	}
	                );
	            },
	            error:function(errodata) {
	            	layer.msg("登录失败", {
	            		icon: 2
	            	}, function(){
	            		$("input").val('');
	                	shakeform();
	            	});
	            }
			});
			return false;
		});
	});
	
    function shakeform() {
        $("#login_form").removeClass('shake_effect');
        setTimeout(function () {
            $("#login_form").addClass('shake_effect')
        }, 1);
    }

	window.onload = function() {
		sessionStorage.removeItem("url");
	}
</script>
</html>
