<html>
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>${titleName}</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	#uploadimg{
    		margin-bottom: 20px;
    	}
    </style>
</head>
<body>
<form id="form1" class="layui-form" action="">
    <fieldset class="layui-elem-field">
        <legend>标题内容部分</legend>
        <table width="96%" align="center">
            <colgroup>
                <col width="10%" align="center"/>
                <col width="50%" align="left"/>
            </colgroup>
            <tr>
                <td>服务标题:</td>
                <td>
                    <input type="hidden" id="key" name="key" value="<#if writing??>${writing.id!''}</#if>">
                    <input type="hidden" id="type" name="type" value="${typeName}">
                    <input id="title" name="title" class="layui-input" type="text" placeholder="请输入标题" autocomplete="off"
                           lay-verify="title" value="<#if writing??>${writing.title!''}</#if>">
                </td>
            </tr>
            <tr>
                <td>标题概述:</td>
                <td>
                    <textarea name="comments" placeholder="请输入内容" class="layui-textarea" lay-verify="comments"><#if writing??>${writing.comments!''}</#if></textarea>
                </td>
            </tr>
            <tr>
                <td>标题配图:</td>
                <td>
                    <button class="layui-btn" id="uploadimg" type="button">上传图片</button>
                    <span>(仅可上传${showUploadFormat!'png,jpg,jpeg'}文件)<span>
                    <div class="layui-progress" lay-filter="imgprogress" lay-showPercent="yes" style="margin-top">
						<div class="layui-progress-bar layui-bg-green" lay-percent="0%"></div>
					</div>
                    <input type="hidden" id="imgUrl" name="imgUrl" lay-verify="imgUrl" value="<#if writing??>${writing.imgUrl!''}</#if>"/>
                </td>
            </tr>
            <tr>
                <td>配图预览:</td>
                <td>
                    <img id="picid" src="<#if writing??>${writing.imgUrl!'/upload/image/none.jpg'}<#else>/upload/image/none.jpg</#if>" width="370" height="260" />
                </td>

            </tr>
        </table>
    </fieldset>
    <fieldset class="layui-elem-field">
    	<#if typeName!='TECHNOLOGY'>
        	<legend>详细内容部分</legend>
        </#if>
        <table width="96%" align="center">
            <colgroup>
                <col width="60%" align="center"/>
            </colgroup>
            <#if typeName!='TECHNOLOGY'>
            <tr>
                <td>
                    <textarea id="note" name="details" lay-verify="details"><#if writing??>${writing.details!''}</#if></textarea>
                </td>
            </tr>
            <#else>
		    	<input type="hidden" name="details" value="&nbsp;">
		    </#if>
            <tr>
                <td>
                    <button id="formsubmit" class="layui-btn" lay-submit lay-filter="save">保存</button>
                </td>
            </tr>
        </table>
    </fieldset>
</form>

</body>
<script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
<script type="application/javascript" src="/webjars/layui/layui.js"></script>
<script type="application/javascript" src="/js/mask.js"></script>
<script type="application/javascript">
    layui.use(['form','upload',<#if typeName!='TECHNOLOGY'>'layedit',</#if>'layer','element'], function(){
        var form = layui.form;
        var upload = layui.upload;
        <#if typeName!='TECHNOLOGY'>
        var layedit = layui.layedit;
        </#if>
        var layer = layui.layer;
        var element = layui.element;

        //表单验证规则自定义
        form.verify({
            title: function(value){
                if(!value) {
                    return '服务标题不能为空';
                }
                if(value.length>100) {
                    return '服务标题长度不能超过100[一个汉字算2个长度]';
                }
            },
            <#if typeName!='TECHNOLOGY'>
            details: function() {
            	var layeditval = layedit.getContent(layeditIndex);
                if(!layeditval || layeditval.length <= 0) {
                    return '详细内容部分不能为空';
                }
            },
            </#if>
            imgUrl: function(value) {
                /*if(!value) {
                    return '请上传标题配图';
                }*/
            }
        });

		var msgtop = document.body.clientHeight / 2;
        var msgleft = document.body.clientWidth / 2;
        //监听提交
        form.on('submit(save)', function(data){
        	<#if typeName!='TECHNOLOGY'>
        	data.field.details=saferHTML(layedit.getContent(layeditIndex));
        	</#if>
        	data.field.title=saferHTML(data.field.title);
        	data.field.comments=saferHTML(data.field.comments);
        	console.log(data.field.comments);
        	debugger
            $.ajax({
                data : data.field,
                type : "POST",
                url : "/mgt/writing/save", //图片上传出来的url，返回的是图片上传后的路径，http格式
                dataType : "json",
                success: function(data) {
                    if(data.code!=0) {
                        layer.msg("保存失败", {
                        	offset: [msgtop+'px', msgleft+'px']
                        });
                        return;
                    }
                    layer.msg("保存成功", {
                    	offset: [msgtop+'px', msgleft+'px']
                    }, function(){
                    	opener.location.reload();
                    	window.close();
                    });
                },
                error:function(data){
	                layer.msg("保存失败", {
	                	offset: [msgtop+'px', msgleft+'px']
	                });
                }
            });
            return false;
        });


        //图片上传
        upload.render({
            elem: '#uploadimg',
            url: '/mgt/upload/pic',
            exts: '${uploadFormat!'png|jpg|jpeg'}', //只允许上传图片
            size: 6000, //限制文件大小，单位 KB
            before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#picid').attr('src', result); //图片链接（base64）
                });
            },
            progress: function(n, elem){
			    var percent = n + '%' //获取进度百分比
			    element.progress('imgprogress', percent); //可配合 layui 进度条元素使用
			},
            done: function(res){
                //如果上传失败
                if(res.code != 0){
                    $('#picid').attr('src', '/upload/image/none.jpg');
                    return layer.msg('上传失败', {
	                       	offset: [msgtop+'px', msgleft+'px']
	                       });
                }
                //上传成功
                $('#imgUrl').val(res.data.src);
            },
            error: function(){
                $('#picid').attr('src', ''); //图片链接（base64）
                return layer.msg('上传失败', {
                        	offset: [msgtop+'px', msgleft+'px']
                        });
            }
        });
        
		<#if typeName!='TECHNOLOGY'>
		layedit.set({
		  uploadImage: {
		    url: '/mgt/upload/pic'
		  }
		});
        var layeditIndex = layedit.build('note', {
			tool: ['strong','italic','underline','del','|','left','center','right'
			  ,'link','unlink','image']
		});
        </#if>
        
        function saferHTML(arg) {
			return arg.replace(/&/g, "&amp;")
		            .replace(/<script>/g, "&lt;script&gt;")
		            .replace(/<\/script>/g, "&lt;/script&gt;");
		}
    });
</script>
</html>
