<html>
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>修改密码</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js"></script>
    <script type="application/javascript" src="/js/mask.js"></script>
    <link rel="stylesheet" href="/css/load/load.css" media="all">
</head>
<body>
<fieldset class="layui-elem-field" style="margin:15px auto;width:95%;">
    <legend>修改密码</legend>
    <div class="layui-form-item">
        <label class="layui-form-label">旧密码：</label>
        <div class="layui-input-inline">
            <input type="password" id="password_old" required lay-verify="required" placeholder="请输入旧密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">新密码：</label>
        <div class="layui-input-inline">
            <input type="password" id="password_new" required lay-verify="required" placeholder="请输入新密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" class="layui-btn" onclick="modifypwd()">更改密码</button>
        </div>
    </div>
</fieldset>
</body>
<script type="application/javascript">
    layui.use('layer', function () {
        var layer = layui.layer;
    });

    function modifypwd() {
        var oldpwd = $('#password_old').val();
        var newpwd = $('#password_new').val();
        if(oldpwd=='') {
            layer.msg("请录入旧密码", {icon: 0, offset: '100px'});
            return;
        }

        if(newpwd=='') {
            layer.msg("请录入新密码", {icon: 0, offset: '100px'});
            return;
        }

        $.ajax({
            url:'/mgt/user/changepwd',
            async:true,
            data:{oldpwd:oldpwd,newpwd:newpwd},
            dataType:'json',
            type:'post',
            success:function(xhrdata) {
                if(0!=xhrdata.code) {
                    layer.msg(xhrdata.msg, {icon: 0, offset: '100px'});
                    return;
                }
                layer.msg("密码修改成功，请重新登录", {icon: 1, offset: '100px'}, function(){
                	window.parent.location.href = '/logout';
                });
            },
            error:function(errodata) {
                layer.msg(errodata.responseText.message, {icon: 0, offset: '100px'});
            }
        });
    }
</script>
</html>
