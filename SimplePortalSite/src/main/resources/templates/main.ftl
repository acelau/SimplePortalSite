<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <title>后台管理</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css"  media="all">
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js" charset="utf-8"></script>
    <script type="application/javascript" src="/js/echarts.min.js"></script>
    <style>
    	.layui-logo{
    		cursor: pointer;
    	}
    	#iframe_web{
    		margin: 5px;
    	}
    	.user_num{
    		color: #0000ff;
    	}
    	.writing_num{
    		color: #ffce3d;
    	}
    </style>
</head>
<body class="layui-layout-body" onbeforeunload="rememberUrl()">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo" onclick="showInfo();">后台管理</div>
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item">
            	<#if userAuth>
	                <a href="javascript:;">权限设置</a>
	                <dl class="layui-nav-child">
	                    <dd><a href="#" onclick="service_web('/mgt/user')">用户管理</a></dd>
	                    <dd><a href="#" onclick="service_web('/mgt/role')">角色管理</a></dd>
	                </dl>
                </#if>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="#">
                    <img src="/img/mgt/loginuser.jpg" class="layui-nav-img">
                    ${loginName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="#" onclick="change_password()">修改密码</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
            	<a href="/logout">
            		<em class="layui-icon layui-icon-logout" style="margin-right:5px;"></em>退出登录
            	</a>
            </li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
            	<li class="layui-nav-item">
            		<a href="#" onclick="service_web('/mgt/writing?type=NEWS')">
            			<em class="layui-icon layui-icon-list" style="margin-right:5px;"></em>新闻维护
            		</a>
            	</li>
                <li class="layui-nav-item">
                	<a href="#" onclick="service_web('/mgt/writing?type=SERVICE')">
                		<em class="layui-icon layui-icon-component" style="margin-right:5px;"></em>服务项目维护
                	</a>
                </li>
                <li class="layui-nav-item">
                	<a href="#" onclick="service_web('/mgt/writing?type=TECHNOLOGY')">
                		<em class="layui-icon layui-icon-app" style="margin-right:5px;"></em>技术应用维护
                	</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <iframe src="" width="100%" frameborder="0" height="99%" id="iframe_web" style="display:none;"></iframe>
        <div style="padding: 20px; background-color: #F2F2F2;" id="info">
	        <div class="layui-row layui-col-space15">
	        	<div class="layui-col-md6">
	        		<div class="layui-card">
				  		<div class="layui-card-header">用户数量</div>
						<div class="layui-card-body">
							<div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-user"></i>
						    	<span>总用户数:</span><span class="user_num">0</span>
						    </div>
						    <div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-face-smile"></i>
						    	<span>正常用户:</span><span class="user_num">0</span>
						    </div>
						    <div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-face-cry"></i>
						    	<span>禁用用户:</span><span class="user_num">0</span>
						    </div>
						    <br>
						</div>
					</div>
	        	</div>
	        	<div class="layui-col-md6">
	        		<div class="layui-card">
				  		<div class="layui-card-header">文章数量</div>
						<div class="layui-card-body">
						    <div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-file"></i>
						    	<span>总文章数:</span><span class="writing_num">0</span>
						    </div>
						    <div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-ok"></i>
						    	<span>显示:</span><span class="writing_num">0</span>
						    </div>
						    <div class="layui-col-md3">
						    	<i class="layui-icon layui-icon-close"></i>
						    	<span>隐藏:</span><span class="writing_num">0</span>
						    </div>
						    <br>
						</div>
					</div>
	        	</div>
	        </div>
	        <div class="layui-row layui-col-space15">
		        <div class="layui-col-md12">
		        	<div class="layui-card">
				  		<div class="layui-card-header">文章统计</div>
				  		<div class="layui-card-body">
				  			<div id="writing_statistics" style="width: 100%;height:400px;"></div>
				  		</div>
				  	</div>
				 </div>
			</div>
	        <div class="layui-row layui-col-space15">
		        <div class="layui-col-md12">
		        	<div class="layui-card">
				  		<div class="layui-card-header">系统信息</div>
						<div class="layui-card-body">
							<table class="layui-table">
							  <tbody>
							  	<tr>
									<th>系统名称</th>
									<td>
										${systemName}
									</td>
									<th>系统版本</th>
									<td>${systemVersion}</td>
								</tr>
								<tr>
									<th>java版本</th>
									<td>${javaVersion}</td>
									<th>java地址</th>
									<td>${javaHome}</td>
								</tr>
								<tr>
									<th>系统架构</th>
									<td>${osArch}</td>
									<th>系统名称</th>
									<td>${osName}</td>
								</tr>
								<tr>
									<th>tomcat</th>
									<td>${serverInfo}</span>
									</td>
									<th>servlet</th>
									<td>${servletVersion}</td>
								</tr>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
	        </div>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        &copy 网站域名 - 底部固定区域
    </div>
</div>
<script>
	var writingChart = echarts.init(document.getElementById('writing_statistics'));
	$(function(){
		var $iframe = $("#iframe_web");
		var url = sessionStorage.getItem("url");
		if(url && url.length>0){
			service_web(url);
		} else {
			showInfo();
		}
		
		writingChart.setOption({
	        title: {
	            text: '文章总数分类统计',
	            left: 'center'
	        },
	        tooltip: {
	        	trigger: 'item',
        		formatter: '{a} <br/>{b} : {c} ({d}%)'
	        },
	        legend: {
	        	orient: 'vertical',
        		left: 'left',
	            data:[]
	        },
	        series: [{
	            name: '数量',
	            type: 'pie',
	            radius: '55%',
            	center: ['50%', '60%'],
	            data: [],
	            emphasis: {
	                itemStyle: {
	                    shadowBlur: 10,
	                    shadowOffsetX: 0,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }]
	    });
	});
    //JavaScript代码区域
    layui.use(['element', 'layer'], function(){
        var element = layui.element;
        var layer = layui.layer;
    });
    
    function change_password(){
    	layer.open({
		  type: 2,
		  title: false,
		  area: ['270px', '240px'],
		  content: '/mgt/modifypwd'
		}); 
    }

    function service_web(url) {
    	$("#info").hide();
    	var $iframe = $("#iframe_web");
        $iframe.attr("src",url);
        $iframe.show();
    }
    function showInfo() {
    	var $iframe = $("#iframe_web");
    	sessionStorage.removeItem("url");
    	$iframe.hide();
    	$iframe.attr("src", "");
    	$iframe.contents().find("body").html("");
    	$("#info").show();
    	$.get("/mgt/statistics/base", function(data){
    		if(data.code != 0){
    			return;
    		}
    		var $userNum = $(".user_num");
    		var $writingNum = $(".writing_num");
    		$.each(data.data.user,function(i,e){
    			$userNum.eq(i).text(e);
    		});
    		$.each(data.data.writing,function(i,e){
    			$writingNum.eq(i).text(e);
    		});
    	});
    	writingChart.showLoading();
    	$.get("/mgt/statistics/writing_type", function(data){
    		if(data.code != 0){
    			return;
    		}
    		writingChart.hideLoading();
    		writingChart.setOption({
    			legend: {
    				data: data.data.name
    			},
    			series: [{
    				data: data.data.data
    			}]
    		});
    	});
    	
    }
    
    function rememberUrl() {
    	var $iframe = $("#iframe_web");
    	var iframe_src = $iframe.attr("src");
		if(iframe_src && iframe_src.length>0){
			sessionStorage.setItem("url", iframe_src);
		}
    }
</script>
</body>
</html>
