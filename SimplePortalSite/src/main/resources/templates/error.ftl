<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <title>错误页面</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link href="/css/error/err.css" rel="stylesheet">
</head>
<body>
	<div class="content">
		<#if err??>
			<p>错误信息：${err.msg}</p>
			<p>错误类型：${err.type}</p>
		</#if>
		<p>抱歉，发生错误了<br>请<a href="#" onclick="window.history.back();" style="color:#ff0000;">返回重试</a>，或联系管理员</p>
	</div>
</body>
</html>
