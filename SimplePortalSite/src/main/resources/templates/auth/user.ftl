<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>用户管理</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js"></script>
    <script type="application/javascript" src="/js/mask.js"></script>
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	fieldset{
    		width: 95%;
    		margin: 2px auto;
    	}
    </style>
</head>
<body>
<fieldset class="layui-elem-field">
    <legend>查询条件</legend>
    <form id="form1" class="layui-form" action="">
        <div class="layui-form-item">
        	<div class="layui-inline">
        		<label class="layui-form-label">用户登录名</label>
			    <div class="layui-input-block">
			    	<input id="loginName" name="loginName" class="layui-input" type="text" placeholder="请输入登录名"
	                           autocomplete="off">
			    </div>
        	</div>
        	<div class="layui-inline">
        		<label class="layui-form-label">用户状态</label>
			    <div class="layui-input-block">
			    	<input id="validflag" name="validFlag" type="checkbox" checked="" lay-skin="switch" value="true"
                           lay-text="启用|禁用">
			    </div>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button class="layui-btn" lay-submit lay-filter="query">查询</button>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
        	</div>
        </div>
    </form>
</fieldset>

<fieldset class="layui-elem-field">
    <legend>查询结果</legend>
    <div class="layui-inline" style="margin:3px 10px;">
    	<#if addPermission>
        	<button class="layui-btn" onclick="add()">新增用户</button>
        </#if>
    </div>
    <table id="gridresult" class="layui-table" lay-filter="gridfilter" lay-data="{url:'/mgt/user/query', page:true, id:'gridresult',height:'630px'}">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox', fixed: 'left',align:'center',hide:true}"></th>
            <th lay-data="{field:'id', width:'5%',hide:true}">用户id</th>
            <th lay-data="{field:'loginName', width:'10%',align:'center'}">登录名</th>
            <th lay-data="{field:'validFlag', width:'10%',align:'center',templet: '#switchTpl'}">用户状态</th>
            <th lay-data="{field:'lastLoginIP', width:'15%',align:'center'}">上次登录IP</th>
            <th lay-data="{field:'lastLoginDate', width:'20%',align:'center'}">上次登录时间</th>
            <th lay-data="{field:'createdDate', width:'20%', sort: true,align:'center'}">创建时间</th>
            <th lay-data="{fixed: 'right',align:'center', toolbar: '#barDemo'}">操作</th>
        </tr>
        </thead>
    </table>

</fieldset>
</body>
<script id="switchTpl" type="text/html">
    <input type="checkbox" name="validflag" value="true" lay-skin="switch" lay-text="启用|禁用" disabled
     {{ d.validFlag?'checked' : '' }}>
</script>
<script id="barDemo" type="text/html">
	<#if editPermission>
		{{#  if(d.loginName != "admin"){ }}
    	<button class="layui-btn layui-btn-xs" lay-event="edit">编辑</button>
    	{{#  } }}
    </#if>
    <#if delPermission>
    	{{#  if(d.loginName != "admin"){ }}
    	<button class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</button>
    	{{#  } }}
    </#if>
</script>
<script type="application/javascript">
    layui.use(['form', 'laydate', 'table'], function () {
        var form = layui.form;
        
        //switch off状态赋值
        form.on('switch', function (data) {
            $(data.elem).attr('type', 'hidden').val(this.checked ? true : false);
        });
        
        //数据表格模块
        var table = layui.table;
        //监听提交
        form.on('submit(query)', function (data) {
            table.reload('gridresult', {
                url: '/mgt/user/query'
                , where: data.field //设定异步数据接口的额外参数
                , page: {
                    page: 1
                }
            });
            return false;
        });
        
        var msgtop = document.body.clientHeight / 2;
        var msgleft = document.body.clientWidth / 2;
        //监听工具条
        table.on('tool(gridfilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
            <#if delPermission>
            	layer.confirm('确认删除该用户?', {icon: 3, title:'提示'}, function(index){
					layer.close(index);
					$.ajax({
	                    type: "DELETE",
	                    url: "/mgt/user/delete",
	                    data: {"id":data.id},
	                    dataType: "json",
	                    success: function (data) {
	                    	if(data.code == 0){
		                    	layer.msg('删除成功', {
				                	icon: 1,
	                    			offset: [msgtop+'px', msgleft+'px']
	                    			},
				                	function(){
		                        		obj.del();
				                	}
				                );
				             } else {
				             	layer.msg(data.msg, {
				            		icon: 2,
				            		offset: [msgtop+'px', msgleft+'px']
				            	});
				             }
	                    },
	                    error: function () {
	                    	layer.msg("删除失败", {
			            		icon: 2,
			            		offset: [msgtop+'px', msgleft+'px']
			            	});
	                    }
	                });
				});
            </#if>
            } else if (obj.event === 'edit') {
            <#if editPermission>
                update(data.id);
            </#if>
            }
        });
    });

	<#if editPermission>
    function update(id) {
        if (!id || id == 0) {
            layer.alert("失败,请重新打开这个菜单");
            return;
        }
        var url = '/mgt/user/edit?id=' + id;
        var name = "用户编辑";
        common(url, name);
    }
	</#if>
	<#if addPermission>
    function add() {
        var url = '/mgt/user/add';
        var name = "用户新增";
        common(url, name);
    }
	</#if>
	function common(url, name) {
		var iWidth = 720;
        var iHeight = 600;
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
        window.open(url, name, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',menubar=0,scrollbars=1, resizable=1,status=1,titlebar=0,toolbar=0,location=1');
    }
</script>
</html>
