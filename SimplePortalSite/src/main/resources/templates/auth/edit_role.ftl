<html>
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>${titleName}</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	tr{
    		line-height: 55px;
    	}
    </style>
</head>
<body>
<form id="form1" class="layui-form" action="">
    <fieldset class="layui-elem-field">
        <legend>角色名称</legend>
        <table width="96%" align="center">
            <colgroup>
                <col width="10%" align="center"/>
                <col width="50%" align="left"/>
            </colgroup>
            <tr>
                <td>名称:</td>
                <td>
                    <input type="hidden" id="key" name="key" value="<#if role??>${role.id!''}</#if>">
                    <input id="roleName" name="roleName" class="layui-input" type="text" placeholder="请输入名称" autocomplete="off"
                           lay-verify="rolename" value="<#if role??>${role.roleName!''}</#if>">
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset class="layui-elem-field">
       	<legend>权限选择</legend>
       	<p style="text-align: center;color: #ff0000;">请注意：用户和角色相关权限，必须勾选“权限设置”</p>
        <table width="96%" align="center">
            <colgroup>
                <col width="60%" align="center"/>
            </colgroup>
            <tr>
                <td>
                	<#list permissions as permission>
                		<input type="checkbox" title="${permission.displayName}" lay-filter="permissionfilter"
                		 value="${permission.id}" <#if role?? && role.permissionIds?seq_contains(permission.id)>checked</#if>> 
                	</#list>
                </td>
            </tr>
            <tr>
                <td>
                    <button id="formsubmit" class="layui-btn" lay-submit lay-filter="save">保存</button>
                </td>
            </tr>
        </table>
    </fieldset>
</form>

</body>
<script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
<script type="application/javascript" src="/webjars/layui/layui.js"></script>
<script type="application/javascript" src="/js/mask.js"></script>
<script type="application/javascript" src="/js/formSubmit.js"></script>
<script type="application/javascript">
    layui.use(['form','layer'], function(){
        var form = layui.form;     
        var layer = layui.layer;

        //表单验证规则自定义
        form.verify({
            rolename: function(value){
                if(!value) {
                    return '角色名称不能为空';
                }
                if(value.length>100) {
                    return '角色名称长度不能超过100[一个汉字算2个长度]';
                }
            }
        });
        var permissionIds = [
        <#if role??>
        <#list role.permissionIds as pid>
        	${pid}
    		<#if pid_has_next>
    		,
    		</#if>
    	</#list>
        </#if>
        ];        
        form.on('checkbox(permissionfilter)', function(data){
        	var $this = $(data.elem);
        	var val = parseInt(data.value);
        	if(data.elem.checked){
        		permissionIds.push(val);
        	}else{
        		var eleIndex = permissionIds.indexOf(val);
        		if(eleIndex!=-1){
        			permissionIds.splice(eleIndex, 1);
        		}
        	}
		});

        //监听提交
        form.on('submit(save)', function(data){
        	data.field.permissionIds = permissionIds;
        	commonFormSubmit(data.field, "/mgt/role/save");
            return false;
        });
    });
</script>
</html>
