<html>
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>${titleName}</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	tr{
    		line-height: 55px;
    	}
    </style>
</head>
<body>
<form id="form1" class="layui-form" action="">
    <fieldset class="layui-elem-field">
        <legend>用户信息</legend>
        <table width="96%" align="center">
            <colgroup>
                <col width="10%" align="center"/>
                <col width="50%" align="left"/>
            </colgroup>
            <tr>
                <td>用户名:</td>
                <td>
                    <input type="hidden" id="key" name="key" value="<#if user??>${user.id!''}</#if>">
                    <input id="loginName" name="loginName" class="layui-input" type="text" placeholder="请输入用户名" autocomplete="off"
                           lay-verify="loginName" value="<#if user??>${user.loginName!''}</#if>" <#if user??>disabled</#if>>
                </td>
            </tr>
            <#if !user??>
	            <tr>
	            	<td>密码:</td>
	                <td>
	                	<input id="password" name="password" class="layui-input" type="password"  lay-verify="password">
	                </td>
	            </tr>
	        <#else>
	        	<input name="password" type="hidden" value="&nbsp;">
	        </#if>
	        <tr>
                <td>角色:</td>
                <td>
                	<input type="hidden" name="roleId" value="<#if user??>${user.role.id}</#if>">
                    <select lay-filter="rolefilter">
					  <option value="">请选择一个角色</option>
					  <#list roles as role>
					  	<option value="${role.id}" <#if user?? && user.role.id == role.id>selected</#if>>${role.roleName}</option>
					  </#list>
					</select>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset class="layui-elem-field">
       	<legend>用户状态</legend>
        <table width="96%" align="center">
            <colgroup>
                <col width="60%" align="center"/>
            </colgroup>
            <tr>
                <td>
                	<input type="radio" name="validFlag" value="true" title="启用" lay-filter="statusfilter" <#if user?? && user.validFlag>checked</#if>>
					<input type="radio" name="validFlag" value="false" title="禁用" lay-filter="statusfilter" <#if user?? && !user.validFlag>checked</#if>>
                </td>
            </tr>
            <tr>
                <td>
                    <button id="formsubmit" class="layui-btn" lay-submit lay-filter="save">保存</button>
                </td>
            </tr>
        </table>
    </fieldset>
</form>

</body>
<script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
<script type="application/javascript" src="/webjars/layui/layui.js"></script>
<script type="application/javascript" src="/js/mask.js"></script>
<script type="application/javascript" src="/js/formSubmit.js"></script>
<script type="application/javascript">
    layui.use(['form','layer'], function(){
        var form = layui.form;     
        var layer = layui.layer;

        //表单验证规则自定义
        form.verify({
            loginName: function(value){
                if(!value) {
                    return '角色名称不能为空';
                }
                if(value.length>100) {
                    return '角色名称长度不能超过100[一个汉字算2个长度]';
                }
            },
            password: function(value){
                if(value.length<6) {
                    return '密码长度不能小于6个字符';
                }
            }
        });
        form.on('select(rolefilter)', function(data){
        	$("input[name='roleId']").val(data.value);
		});
		form.on('radio(statusfilter)', function(data){
		  $(data.elem).attr("checked","checked");
		});
		
        //监听提交
        form.on('submit(save)', function(data){
        	commonFormSubmit(data.field, "/mgt/user/save");
            return false;
        });
    });
</script>
</html>
