<html lang="zh-cn">
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta charset="utf-8">
    <title>角色管理</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="bookmark" href="/favicon.ico"/>
    <link rel="stylesheet" href="/webjars/layui/css/layui.css" media="all">
    <script type="application/javascript" src="/webjars/jquery/jquery.js"></script>
    <script type="application/javascript" src="/webjars/layui/layui.js"></script>
    <script type="application/javascript" src="/js/mask.js"></script>
    <link rel="stylesheet" href="/css/load/load.css" media="all">
    <style>
    	fieldset{
    		width: 95%;
    		margin: 2px auto;
    	}
    </style>
</head>
<body>
<fieldset class="layui-elem-field">
    <legend>查询条件</legend>
    <form id="form1" class="layui-form" action="">
        <div class="layui-form-item">
        	<div class="layui-inline">
        		<label class="layui-form-label">角色名称</label>
			    <div class="layui-input-block">
			    	<input id="rName" name="rName" class="layui-input" type="text" placeholder="请输入名称"
	                           autocomplete="off">
			    </div>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button class="layui-btn" lay-submit lay-filter="query">查询</button>
        	</div>
        	<div class="layui-inline" style="float:right;">
        		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
        	</div>
        </div>
    </form>
</fieldset>

<fieldset class="layui-elem-field">
    <legend>查询结果</legend>
    <div class="layui-inline" style="margin:3px 10px;">
    	<#if addPermission>
        	<button class="layui-btn" onclick="add()">新增角色</button>
        </#if>
    </div>
    <table id="gridresult" class="layui-table" lay-filter="gridfilter" lay-data="{url:'/mgt/role/query', page:true, id:'gridresult',height:'630px'}">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox', fixed: 'left',align:'center',hide:true}"></th>
            <th lay-data="{field:'id', width:'5%',hide:true}">角色id</th>
            <th lay-data="{field:'roleName', width:'40%',align:'center'}">角色名称</th>
            <th lay-data="{field:'createdDate', width:'20%', sort: true,align:'center'}">创建时间</th>
            <th lay-data="{fixed: 'right',align:'center', toolbar: '#barDemo'}">操作</th>
        </tr>
        </thead>
    </table>

</fieldset>
</body>
<script id="barDemo" type="text/html">
	<#if editPermission>
		{{#  if(d.roleName != "管理员"){ }}
    	<button class="layui-btn layui-btn-xs" lay-event="edit">编辑</button>
    	{{#  } }}
    </#if>
    <#if delPermission>
    	{{#  if(d.roleName != "管理员"){ }}
    	<button class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</button>
    	{{#  } }}
    </#if>
</script>
<script type="application/javascript">
    layui.use(['form', 'laydate', 'table'], function () {
        var form = layui.form;
        //数据表格模块
        var table = layui.table;
        //监听提交
        form.on('submit(query)', function (data) {
            table.reload('gridresult', {
                url: '/mgt/role/query'
                , where: data.field //设定异步数据接口的额外参数
                , page: {
                    page: 1
                }
            });
            return false;
        });
        
        var msgtop = document.body.clientHeight / 2;
        var msgleft = document.body.clientWidth / 2;
        //监听工具条
        table.on('tool(gridfilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
            <#if delPermission>
            	layer.confirm('删除前请确认已没有用户持有该角色，否则用户会失去权限！', {icon: 3, title:'提示'}, function(index){
					layer.close(index);
					$.ajax({
	                    type: "DELETE",
	                    url: "/mgt/role/delete",
	                    data: {"id":data.id},
	                    dataType: "json",
	                    success: function (data) {
	                    	if(data.code == 0){
		                    	layer.msg('删除成功', 
				                	{icon: 1,offset: [msgtop+'px', msgleft+'px']},
				                	function(){
		                        		obj.del();
				                	}
				                );
				             } else {
				             	layer.msg(data.msg, {
				            		icon: 2,
				            		offset: [msgtop+'px', msgleft+'px']
				            	});
				             }
	                    },
	                    error: function () {
	                    	layer.msg("删除失败", {
			            		icon: 2,
			            		offset: [msgtop+'px', msgleft+'px']
			            	});
	                    }
	                });
				});
            </#if>
            } else if (obj.event === 'edit') {
            <#if editPermission>
                update(data.id);
            </#if>
            }
        });
    });

	<#if editPermission>
    function update(id) {
        if (!id || id == 0) {
            layer.alert("失败,请重新打开这个菜单");
            return;
        }
        var url = '/mgt/role/edit?id=' + id;
        var name = "角色编辑";
        common(url, name);
    }
	</#if>
	<#if addPermission>
    function add() {
        var url = '/mgt/role/add';
        var name = "角色新增";
        common(url, name);
    }
	</#if>
	function common(url, name) {
		var iWidth = 720;
        var iHeight = 600;
        var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
        var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
        window.open(url, name, 'height=' + iHeight + ',innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',menubar=0,scrollbars=1, resizable=1,status=1,titlebar=0,toolbar=0,location=1');
    }
</script>
</html>
