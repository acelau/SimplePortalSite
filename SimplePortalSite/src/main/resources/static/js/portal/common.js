/**
 * 响应式小屏导航栏按钮
 */
$(".mobile-icon").click(function () {
	console.log("c")
   $(".nav-list").toggleClass('layui-show');
});
/**
 * 绘制二维码
 */
 var qrcode = new QRCode(document.getElementById("qrcode"), {
	text : window.location.href,
	width : 86,
	height : 86,
	colorDark : "#000000",
	colorLight : "#ffffff",
	correctLevel : QRCode.CorrectLevel.H
});