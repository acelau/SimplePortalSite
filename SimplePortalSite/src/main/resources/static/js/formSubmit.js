/**
 * 公共表单提交
 */
var msgtop = document.body.clientHeight / 2;
var msgleft = document.body.clientWidth / 2;
function commonFormSubmit(senddata, url) {
	$.ajax({
		data: senddata,
		type: "POST",
		url: url,
		dataType: "json",
		success: function(data) {
			if (data.code != 0) {
				layer.msg(data.msg, {
					offset: [msgtop + 'px', msgleft + 'px']
				});
				return;
			}
			layer.msg(data.msg, {
				offset: [msgtop + 'px', msgleft + 'px']
			}, function() {
				opener.location.reload();
				window.close();
			});
		},
		error: function() {
			layer.msg("保存出错！", {
				offset: [msgtop + 'px', msgleft + 'px']
			});
		}
	});
}