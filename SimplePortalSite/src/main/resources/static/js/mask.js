/**
 * 
 */
$(function() {
	$("<div id='mask'></div>").appendTo("body");
	var $mask = $("#mask");
	$mask.css({
		"display": "none",
		"width": "100%",
		"height": "100%",
		"z-index": "9999",
		"background-color": "rgba(242,242,242, 0.5)"
	});
	$mask.append("<div>数据加载中...</div>");
	
	var $document = $(document);
	$document.ajaxStart(function(){
        $mask.show();
    });
    $document.ajaxStop(function(){
        $mask.hide();
    });
});