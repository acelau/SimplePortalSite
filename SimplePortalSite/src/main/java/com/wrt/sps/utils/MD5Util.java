package com.wrt.sps.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang3.StringUtils;

import lombok.extern.log4j.Log4j2;

/**
 * MD5加密工具类
 * 
 * @author 文瑞涛
 * @date 2020年12月13日 下午1:47:31
 */
@Log4j2
public class MD5Util {

	private MD5Util() {
		
	}

	/**
     * MD5加盐
     * 
     * 待加密的字符串加上“盐值”，然后进行 MD5 加密
     *
     * @param string 需要加密的数据
     * @param salt 盐
     * @return 加密后的数据
     */
    public static String encrypt(String string, String salt) {
        if (StringUtils.isEmpty(string)) {
            return "";
        }
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] bytes;
            if(StringUtils.isBlank(salt)) {
            	bytes = md5.digest(string.getBytes());
            } else {
            	bytes = md5.digest((string + salt).getBytes());
            }
            StringBuilder result = new StringBuilder();
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result.append(temp);
            }
            return result.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
}
