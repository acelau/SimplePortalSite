package com.wrt.sps.web.service.impl;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.wrt.sps.mgt.entity.Writing;
import com.wrt.sps.mgt.entity.Writing.Type;
import com.wrt.sps.web.dao.WritingQueryDao;
import com.wrt.sps.web.service.WritingQueryService;

/**
 * 文章查询服务实现层
 * 
 * @author 文瑞涛
 * @date 2021年1月4日 下午9:29:28
 */
@Service
public class WritingQueryServiceImpl implements WritingQueryService {

	@Resource
	private WritingQueryDao dao;

	@Override
	public Page<Writing> query(Type type, int page, int limit) {
		Pageable pageable = PageRequest.of(page, limit, Sort.by(Sort.Direction.DESC, "createdDate"));
		return dao.findWriting(type, pageable);
	}

	@Override
	public Writing getDetails(Long id) {
		Optional<Writing> result = dao.findById(id);
		if (result.isPresent()) {
			return result.get();
		} else {
			return null;
		}
	}
}
