package com.wrt.sps.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wrt.sps.mgt.entity.UploadFile;

/**
 * 上传文件查询数据层
 * 
 * @author 文瑞涛
 * @date 2021年8月8日 下午2:53:36
 */
public interface UploadFileQueryDao extends JpaRepository<UploadFile, Long> {

	/**
	 * 通过文件编号查询上传的文件
	 * 
	 * @param code 文件编号
	 * @return 上传文件对象
	 */
	public UploadFile findByCode(String code);
}
