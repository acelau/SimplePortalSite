package com.wrt.sps.web.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wrt.sps.mgt.entity.Writing;

/**
 * 文章查询数据层
 * 
 * @author 文瑞涛
 * @date 2021年1月4日 下午9:11:35
 */
public interface WritingQueryDao extends JpaRepository<Writing, Long>{

	/**
	 * 
	 * 分页查询文章
	 * 
	 * @param type 文章类型
	 * @param pageable 分页条件
	 * 
	 * @return 分页结果
	 */
	@Query(value = "select new Writing (id,title,comments,imgUrl,createdDate) from Writing where validFlag=true and type=:type",
            countQuery = "select count(id) from Writing where validFlag=true and type=:type")
    public Page<Writing> findWriting(@Param("type")Writing.Type type, Pageable pageable);
}
