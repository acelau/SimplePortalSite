package com.wrt.sps.web.service;

import com.wrt.sps.mgt.entity.UploadFile;

/**
 * 上传文件查询服务层
 * 
 * @author 文瑞涛
 * @date 2021年8月8日 下午3:04:29
 */
public interface UploadFileQueryService {

	/**
	 * 通过文件编号获取上传文件
	 * 
	 * @param code 文件编号
	 * @return 上传文件对象
	 */
	public UploadFile getUploadFile(String code);
}
