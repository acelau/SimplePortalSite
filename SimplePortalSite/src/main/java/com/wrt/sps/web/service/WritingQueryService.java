package com.wrt.sps.web.service;

import org.springframework.data.domain.Page;

import com.wrt.sps.mgt.entity.Writing;

/**
 * 文章查询服务层
 * 
 * @author 文瑞涛
 * @date 2021年1月4日 下午9:26:51
 */
public interface WritingQueryService {

	/**
	 * 
	 * 分页查询文章
	 * 
	 * @param type 文章类型
	 * @param page 当前页码
	 * @param limit 每页显示条数
	 * 
	 * @return 分页查询结果
	 */
	Page<Writing> query(Writing.Type type, int page, int limit);

	/**
	 * 
	 * 通过主键查询文章
	 * 
	 * @param id 文章主键
	 * 
	 * @return 文章
	 */
	Writing getDetails(Long id);
}
