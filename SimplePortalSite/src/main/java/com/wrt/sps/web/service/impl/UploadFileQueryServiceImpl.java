package com.wrt.sps.web.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wrt.sps.mgt.entity.UploadFile;
import com.wrt.sps.web.dao.UploadFileQueryDao;
import com.wrt.sps.web.service.UploadFileQueryService;

/**
 * 上传文件查询服务实现层
 * 
 * @author 文瑞涛
 * @date 2021年8月8日 下午3:11:24
 */
@Service
public class UploadFileQueryServiceImpl implements UploadFileQueryService {
	
	@Resource
	private UploadFileQueryDao uploadFileQueryDao;

	@Override
	public UploadFile getUploadFile(String code) {
		return uploadFileQueryDao.findByCode(code);
	}

	
}
