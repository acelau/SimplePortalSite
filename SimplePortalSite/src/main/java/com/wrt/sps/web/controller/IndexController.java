package com.wrt.sps.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 网站主页接口
 * 
 * @author 文瑞涛
 * @date 2021年1月4日 上午9:45:14
 */
@Controller
public class IndexController {

	@GetMapping({ "/", "/html", "/html/", "/html/index" })
	public String index() {
		return "forward:/html/index.html";
	}
}
