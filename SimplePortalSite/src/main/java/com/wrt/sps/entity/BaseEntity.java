package com.wrt.sps.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Resolution;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

/**
 * @Description 公共基础实体类
 * @author 文瑞涛
 * @date 2020年3月24日 下午2:22:33
 */
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1422950533379121185L;

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 创建日期
	 */
	@CreatedDate
	@DateBridge(resolution = Resolution.SECOND)
	@Column(nullable = false, updatable = false)
	private Date createdDate;
	
	/**
	 * 修改日期
	 */
	@LastModifiedDate
	@DateBridge(resolution = Resolution.SECOND)
	@Column(nullable = true)
	private Date lastModifiedDate;
	
	/**
	 * 版本
	 */
	@Version
	@Column(nullable = false)
	private Long version;
}
