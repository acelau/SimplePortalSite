package com.wrt.sps.entity;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

import lombok.Getter;
import lombok.Setter;

/**
 * 结果
 * 
 * @author 文瑞涛
 */
@Setter
@Getter
public final class Results implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4416553587959564974L;

	private static final String SUCCESS_MSG = "成功";

	private static final String FAIL_MSG = "失败";
	/**
	 * 成功（0）或失败（1）代码
	 */
	private int code;
	/**
	 * 消息
	 */
	private String msg;
	/**
	 * 数据
	 */
	private Object data;

	/**
	 * 
	 * 返回成功信息
	 * 
	 * @param msg 消息
	 * @param data 数据
	 * 
	 * @return 结果
	 *
	 */
	public static Results success(String msg, Object data) {
		Results success = new Results();
		success.setCode(0);
		success.setMsg(StringUtils.isNotBlank(msg) ? msg : SUCCESS_MSG);
		success.setData(data);
		return success;
	}

	/**
	 * 
	 * 返回失败信息
	 * 
	 * @param msg 消息
	 * @param data 数据
	 * 
	 * @return 结果
	 *
	 */
	public static Results fail(String msg, Object data) {
		Results success = new Results();
		success.setCode(1);
		success.setMsg(StringUtils.isNotBlank(msg) ? msg : FAIL_MSG);
		success.setData(JSON.toJSONString(data));
		return success;
	}
}