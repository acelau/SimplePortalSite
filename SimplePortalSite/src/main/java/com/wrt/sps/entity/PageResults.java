package com.wrt.sps.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;

import lombok.Data;

/**
 * 带有分页信息的结果实体
 * @author 文瑞涛
 * @date 2020年12月20日 下午8:51:53
 */
@Data
public class PageResults implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -437909830213737477L;
	/**
	 * 数据集合
	 */
	private List<?> data;
	/**
	 * 总数
	 */
    private long count;
    /**
     * 成功（0）或失败（1）代码
     */
    private int code;
    /**
     * 消息
     */
    private String msg;
    
    public PageResults(Page<?> page) {
    	if(null != page) {
    		this.code = 0;
    		this.data = page.getContent();
    		this.count = page.getTotalElements();
    		this.msg = "查询成功";
    	} else {
    		this.code = 1;
    		this.msg = "查询失败";
    		this.data = Collections.emptyList();
    		this.count = 0L;
    	}
    }
}
