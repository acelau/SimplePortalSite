package com.wrt.sps.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 全局异常处理
 * 
 * @author 文瑞涛
 * @date 2021年1月11日 下午2:32:28
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public String resultException(Exception e, Model model) {
		Map<String, String>err = new HashMap<>(2);
		err.put("msg", e.getMessage());
		err.put("type", e.getClass().getName());
		model.addAttribute("err", err);
		return "error";
	}
}
