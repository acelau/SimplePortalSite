package com.wrt.sps.config;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.salt.RandomSaltGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEByteEncryptor;
import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEStringEncryptor;

@Configuration
public class Encryptor {

    @Bean
    StringEncryptor jasyptStringEncryptor() {
        SimplePBEByteEncryptor sp = new SimplePBEByteEncryptor();
        sp.setPassword("abc");
        sp.setAlgorithm("PBEWithMD5AndDES");
        sp.setSaltGenerator(new RandomSaltGenerator());
        sp.setIterations(1000);
        return new SimplePBEStringEncryptor(sp);
    }
}
