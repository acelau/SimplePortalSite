package com.wrt.sps.mgt.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.wrt.sps.mgt.entity.Role;
import com.wrt.sps.mgt.pojo.RoleDTO;

/**
 * 角色服务层
 * 
 * @author 文瑞涛
 * @date 2021年1月7日 上午11:58:17
 */
public interface RoleService {
	/**
	 * 初始化管理员角色
	 */
	public void initAdminRole();

	/**
	 * 
	 * 通过条件查询角色
	 * 
	 * @param role 角色
	 * 
	 * @return 符合条件的角色
	 */
	public Role findRoleByExample(Role role);

	/**
	 * 
	 * 检查角色名称是否已被使用
	 * 
	 * @param rName 角色名称
	 * 
	 * @return 是否被使用
	 */
	public boolean checkRoleExistByExample(String rName);

	/**
	 * 
	 * 通过主键查询角色
	 * 
	 * @param id 角色主键
	 * 
	 * @return 角色
	 */
	public Role findRoleById(Long id);

	/**
	 * 
	 * 分页查询角色
	 * 
	 * @param rName 角色名称
	 * @param page 当前页码
	 * @param limit 每页显示条数
	 * 
	 * @return 分页查询结果
	 */
	public Page<Role> query(String rName, int page, int limit);

	/**
	 * 
	 * 获取系统内所有角色
	 * 
	 * @return 角色集合
	 */
	public List<Role> getAllRoles();

	/**
	 * 
	 * 保存角色
	 * 
	 * @param dto 角色数据
	 * 
	 * @return 保存后的角色
	 */
	public Role saveRole(RoleDTO dto);

	/**
	 * 
	 * 删除角色
	 * 
	 * @param id 角色主键
	 */
	public void delete(Long id);
}
