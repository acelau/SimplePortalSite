package com.wrt.sps.mgt.page.controller;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wrt.sps.mgt.service.PermissionService;
import com.wrt.sps.mgt.service.RoleService;

@Controller
@RequestMapping("/mgt/role")
public class RolePageController extends CommonPermission {

	@Resource
	private RoleService roleService;
	
	@Resource
	private PermissionService permissionService;
	
	/**
	 * 
	 * 添加角色（页面跳转）
	 * 
	 * @param model
	 * 
	 * @return 页面地址
	 */
	@RequiresPermissions("role_add")
	@GetMapping("/add")
	public String add(Model model) {
		commonHandle(model);
		model.addAttribute("titleName", "新增角色");
		return "auth/edit_role";
	}

	/**
	 * 
	 * 编辑角色（页面跳转）
	 * 
	 * @param 角色主键  id
	 * @param model
	 * 
	 * @return 页面地址
	 */
	@RequiresPermissions("role_edit")
	@GetMapping("/edit")
	public String edit(Long id, Model model) {
		if (null == id || id == 0) {
			return null;
		}
		commonHandle(model);
		model.addAttribute("role", roleService.findRoleById(id));
		model.addAttribute("titleName", "更新角色");
		return "auth/edit_role";
	}
	
	/**
	 * 
	 * 添加和编辑页面所需载入的系统权限
	 * 
	 * @param model
	 */
	private void commonHandle(Model model) {
		model.addAttribute("permissions", permissionService.getAllPermissions());
	}
}
