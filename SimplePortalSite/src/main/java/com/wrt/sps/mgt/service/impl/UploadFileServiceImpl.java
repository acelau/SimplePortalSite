package com.wrt.sps.mgt.service.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wrt.sps.mgt.dao.UploadFileDao;
import com.wrt.sps.mgt.entity.UploadFile;
import com.wrt.sps.mgt.service.UploadFileService;

/**
 * 上传文件服务实现层
 * 
 * @author 文瑞涛
 * @date 2021年4月11日 上午9:23:34
 */
@Service
public class UploadFileServiceImpl implements UploadFileService {

	@Resource
	private UploadFileDao dao;
	
	@Override
	@Transactional(readOnly = true)
	public UploadFile findByCode(String code) {
		return dao.findByCode(code);
	}

	@Override
	public List<UploadFile> findNotUsedFile() {
		return dao.findByUsedFalse();
	}
	
	@Override
	@Transactional
	public UploadFile save(UploadFile uploadFile) {
		UploadFile uf = findByCode(uploadFile.getCode());
		if(null != uf) {
			uf.setUsed(uploadFile.getUsed());
			return dao.save(uf);
		} else {
			return dao.save(uploadFile);
		}
	}

	@Override
	public void delete(Long id) {
		Optional<UploadFile> uf = dao.findById(id);
		if(uf.isPresent() && !uf.get().getUsed().booleanValue()) {
			dao.deleteById(id);
		}
	}


}
