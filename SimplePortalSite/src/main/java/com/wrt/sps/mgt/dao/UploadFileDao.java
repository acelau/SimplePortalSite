package com.wrt.sps.mgt.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wrt.sps.mgt.entity.UploadFile;

/**
 * 上传文件数据层
 * 
 * @author 文瑞涛
 * @date 2021年4月10日 下午4:18:28
 */
public interface UploadFileDao extends CrudRepository<UploadFile, Long> {

	/**
	 * 通过上传文件编号查询文件对象
	 * 
	 * @param code 文件编号
	 * 
	 * @return 上传文件对象
	 */
	public UploadFile findByCode(String code);
	
	/**
	 * 查找所有未使用的上传文件
	 * 
	 * @return 未使用上传文件集合
	 */
	public List<UploadFile> findByUsedFalse();
}
