package com.wrt.sps.mgt.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.wrt.sps.mgt.entity.Writing;
import com.wrt.sps.mgt.pojo.WritingDTO;
import com.wrt.sps.mgt.pojo.WritingQueryDTO;

/**
 * 文章管理服务层
 * 
 * @author 文瑞涛
 * @date 2020年12月19日 下午5:12:23
 */
public interface WritingService {

	/**
	 * 
	 * 分页查询文章
	 * 
	 * @param dto 文章查询数据
	 * @param page 当前页码
	 * @param limit 每页显示条数
	 * 
	 * @return 分页查询结果
	 */
	public Page<Writing> query(WritingQueryDTO dto, int page, int limit);

	/**
	 * 
	 * 通过主键查询文章
	 * 
	 * @param id 文章主键
	 * 
	 * @return 文章
	 */
	public Writing findById(Long id);
	
	/**
	 * 
	 * 统计当前系统内的文章数量
	 * 
	 * @return 数量集合
	 */
	public List<Long> countWritings();
	
	/**
	 * 
	 * 统计当前系统内的分类文章数量
	 * 
	 * @return 数量集合
	 */
	public Map<Writing.Type, Long> countWritingsByType();

	/**
	 * 
	 * 保存文章
	 * 
	 * @param dto 文章数据
	 * 
	 * @return 保存后的文章
	 */
	public Writing save(WritingDTO dto);

	/**
	 * 
	 * 删除文章
	 * 
	 * @param id 文章主键
	 */
	public void delete(Long id);

	/**
	 * 
	 * 调整文章是否显示
	 * 
	 * @param id 文章主键
	 * @param isShow 是否显示
	 * 
	 * @return 修改后的文章
	 */
	public Writing display(Long id, boolean isShow);
}
