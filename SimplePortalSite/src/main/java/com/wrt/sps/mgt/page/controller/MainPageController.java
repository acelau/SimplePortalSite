package com.wrt.sps.mgt.page.controller;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wrt.sps.mgt.entity.User;

@Controller
@RequestMapping("/mgt")
public class MainPageController {

	@Value("${system.name}")
	private String systemName;
	@Value("${system.version}")
	private String systemVersion;
	
	@Resource
	private ServletContext servletContext;
	
	/**
	 * 后台管理首页
	 */
	@GetMapping("/main")
	public String main(Model model) {
		Subject subject = SecurityUtils.getSubject();
		User user = (User) subject.getPrincipal();
		model.addAttribute("loginName", user.getLoginName());
		model.addAttribute("userAuth", subject.isPermitted("user_authority"));
		model.addAttribute("systemName", systemName);
		model.addAttribute("systemVersion", systemVersion);
		model.addAttribute("javaVersion", System.getProperty("java.version"));
		model.addAttribute("javaHome", System.getProperty("java.home"));
		model.addAttribute("osName", System.getProperty("os.name"));
		model.addAttribute("osArch", System.getProperty("os.arch"));
		model.addAttribute("serverInfo", servletContext.getServerInfo());
		model.addAttribute("servletVersion", servletContext.getMajorVersion() + "." + servletContext.getMinorVersion());
		return "main";
	}
	
	/**
	 * 修改密码
	 */
	@GetMapping("/modifypwd")
	public String modifypwd() {
		return "changepwd";
	}
}
