package com.wrt.sps.mgt.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.wrt.sps.mgt.entity.Writing;
import lombok.Data;

/**
 * 文章数据实体类
 * 
 * @author 文瑞涛
 * @date 2020年12月19日 下午5:08:41
 */
@Data
public class WritingDTO {
	
	private static String urlflag = "/web/file/";
	/**
	 * 文章主键
	 */
	private Long key;
	/**
	 * 文章标题
	 */
	@NotBlank(message = "标题不可为空")
	private String title;
	/**
	 * 文章简介
	 */
	@NotBlank(message = "概述不可为空")
	private String comments;
	/**
	 * 图片地址
	 */
	private String imgUrl;
	/**
	 * 文章详情
	 */
	@NotBlank(message = "详情不可为空")
	private String details;
	/**
	 * 文章类型
	 */
	@NotNull(message = "类型异常")
	private Writing.Type type;
	
	/**
	 * 获取图片编号
	 * 
	 * @return 图片编号
	 */
	public String getImgCode() {
		return getImgCode(this.imgUrl);
	}
	
	/**
	 * 通过图片url地址获取图片编号
	 * 
	 * @param imgUrl 图片url地址
	 * 
	 * @return 图片编号
	 */
	public static String getImgCode(String imgUrl) {
		if(StringUtils.isNotBlank(imgUrl)) {
			return imgUrl.replace(urlflag, StringUtils.EMPTY);
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	/**
	 * 获取详情中的图片编号
	 * 
	 * @return 图片编号集合
	 */
	public List<String> getImgCodesInDetails(){
		return getImgCodesInDetails(this.details);
	}
	
	/**
	 * 获取详情中的图片编号
	 * 
	 * @param details 详情
	 * 
	 * @return 图片编号集合
	 */
	public static List<String> getImgCodesInDetails(String details){
		int start = 0;
		int index = 0;
		int uuidlength = 36;
		List<String> codeList = new ArrayList<>();
		while (index < details.length() - 1) {
			index = details.indexOf(urlflag, start);
			if (index == -1) {
				break;
			}
			int sindex = index + urlflag.length();
			String temp = details.substring(sindex, sindex + uuidlength);
			codeList.add(temp);
			start = sindex + uuidlength;
		}
		return codeList;
	}
	
	public void setTitle(String title) {
		this.title = safeString(title);
	}
	
	public void setComments(String comments) {
		this.comments = safeString(comments);
	}
	
	public void setDetails(String details) {
		this.details = safeString(details);
	}
	
	/**
	 * 防御xss攻击
	 * 
	 * @param string 字符串属性值
	 * 
	 * @return 转义后的字符串
	 */
	public static String safeString(String string) {
		String regex = "/web/file/\\w{8}(-\\w{4}){3}-\\w{12}";
        Pattern p = Pattern.compile(regex);
		Document doc = Jsoup.parse(string);
		Elements eles = doc.getElementsByTag("img");
        for (Element element : eles) {
            String src = element.attr("src");
            Matcher tempm = p.matcher(src);
            if(!tempm.find()){
                element.attr("src", "/upload/image/none.jpg");
            }
        }
        string = doc.getElementsByTag("body").first().html();
		return string.replace("&", "&amp;")
	            .replace("<script>", "&lt;script&gt;")
	            .replace("</script>", "&lt;/script&gt;")
	            .replace("<iframe>", "&lt;iframe&gt;")
	            .replace("</iframe>", "&lt;/iframe&gt;")
	            .replace("<a>", "&lt;a&gt;")
	            .replace("</a>", "&lt;/a&gt;");
	}
}
