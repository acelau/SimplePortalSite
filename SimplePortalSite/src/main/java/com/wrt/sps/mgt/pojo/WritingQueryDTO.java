package com.wrt.sps.mgt.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wrt.sps.mgt.entity.Writing;

import lombok.Data;

/**
 * 文章查询实体类
 * 
 * @author 文瑞涛
 * @date 2020年12月20日 下午7:04:32
 */
@Data
public class WritingQueryDTO {
	/**
	 * 文章标题
	 */
	private String title;
	/**
	 * 文章创建起始日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date starttime;
	/**
	 * 文章创建结束日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endtime;
	/**
	 * 文章是否显示
	 */
	private Boolean validFlag;
	/**
	 * 文章类型
	 */
	private Writing.Type type;

}
