package com.wrt.sps.mgt.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wrt.sps.entity.BaseEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * 角色实体类
 * 
 * @author 文瑞涛
 * @date 2021年1月6日 下午9:55:03
 */
@Entity
@Getter
@Setter
public class Role extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7699449996514845437L;

	public static final String ADMIN_ROLE_NAME = "管理员";

	/**
	 * 角色名称
	 */
	@Column(nullable = false, unique = true)
	private String roleName;

	/**
	 * 角色对应权限集合
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Permission> permissions;

	/**
	 * 重写获取创建日期
	 */
	@Override
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getCreatedDate() {
		return super.getCreatedDate();
	}

	/**
	 * 获取当前角色所拥有的权限主键
	 */
	public Long[] getPermissionIds() {
		if (this.permissions.isEmpty()) {
			return ArrayUtils.EMPTY_LONG_OBJECT_ARRAY;
		} else {
			return this.permissions.stream().map(Permission::getId).toArray(Long[]::new);
		}
	}

	/**
	 * 创建管理员角色
	 */
	public void createAdminRole() {
		this.setRoleName(ADMIN_ROLE_NAME);
	}
}
