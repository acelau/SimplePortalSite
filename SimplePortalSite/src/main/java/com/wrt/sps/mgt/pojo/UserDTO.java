package com.wrt.sps.mgt.pojo;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 用户数据实体类
 * 
 * @author 文瑞涛
 * @date 2020年12月19日 下午5:08:41
 */
@Data
public class UserDTO {

	/**
	 * 主键
	 */
	private Long key;
	/**
	 * 登录名
	 */
	@NotBlank(message = "用户名不可为空")
	private String loginName;
	/**
	 * 密码
	 */
	@NotBlank(message = "密码不可为空")
	private String password;
	/**
	 * 是否启用
	 */
	private Boolean validFlag;
	/**
	 * 角色主键
	 */
	private Long roleId;

}
