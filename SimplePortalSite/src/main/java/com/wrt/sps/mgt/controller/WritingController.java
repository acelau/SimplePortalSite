package com.wrt.sps.mgt.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wrt.sps.entity.PageResults;
import com.wrt.sps.entity.Results;
import com.wrt.sps.mgt.entity.UploadFile;
import com.wrt.sps.mgt.entity.Writing;
import com.wrt.sps.mgt.event.UploadFileEvent;
import com.wrt.sps.mgt.pojo.WritingDTO;
import com.wrt.sps.mgt.pojo.WritingQueryDTO;
import com.wrt.sps.mgt.service.WritingService;

/**
 * 文章管理接口
 * 
 * @author 文瑞涛
 * @date 2020年12月19日 下午2:48:20
 */
@Controller
@RequestMapping("/mgt/writing")
public class WritingController {
	
	@Autowired
	private ApplicationEventPublisher publisher;

	@Resource
	private WritingService writingService;
	
	/**
	 * 
	 * 分页查询文章
	 * 
	 * @param 文章查询信息 dto
	 * @param 当前页码   page
	 * @param 每页显示条数 limit
	 * 
	 * @return 分页结果
	 */
	@GetMapping("/query")
	@ResponseBody
	public PageResults query(WritingQueryDTO dto, @RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		return new PageResults(writingService.query(dto, page - 1, limit));
	}

	/**
	 * 
	 * 保存文章
	 * 
	 * @param 文章数据   dto
	 * @param 校验错误结果 bindingResult
	 * 
	 * @return 保存结果
	 */
	@RequiresPermissions(value = { "writing_add", "writing_edit" }, logical = Logical.OR)
	@PostMapping("/save")
	@ResponseBody
	public Results save(@Validated WritingDTO dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			FieldError fieldError = bindingResult.getFieldError();
			return Results.fail(null != fieldError ? fieldError.getDefaultMessage() : null, null);
		}
		Writing temp = writingService.save(dto);
		if (null != temp) {
			publisher.publishEvent(new UploadFileEvent(this, new UploadFile(dto.getImgCode(), true)));
			List<String> imgCodeInDetails = dto.getImgCodesInDetails();
			if (!imgCodeInDetails.isEmpty()) {
				for (String imgcode : imgCodeInDetails) {
					publisher.publishEvent(new UploadFileEvent(this, new UploadFile(imgcode, true)));
				}
			}
			return Results.success(null, temp);
		} else {
			return Results.fail(null, null);
		}
	}

	/**
	 * 
	 * 删除文章
	 * 
	 * @param 文章主键 id
	 * 
	 * @return 删除结果
	 */
	@RequiresPermissions("writing_del")
	@DeleteMapping("/delete")
	@ResponseBody
	public Results delete(Long id) {
		if (null == id) {
			return Results.fail("参数错误，删除失败", null);
		}
		Writing temp = writingService.findById(id);
		if (null != temp) {
			if (temp.getValidFlag().booleanValue()) {
				return Results.fail("展示中，不可删除", null);
			} else {
				writingService.delete(id);
				return Results.success(null, null);
			}
		} else {
			return Results.fail("已删除", null);
		}
	}

	/**
	 * 
	 * 文章是否显示
	 * 
	 * @param 文章主键   id
	 * @param 文章是否显示 isShow
	 *
	 * @return 处理结果
	 */
	@RequiresPermissions("writing_edit")
	@PostMapping("/display")
	@ResponseBody
	public Results display(Long id, boolean isShow) {
		if (null == id) {
			return Results.fail("参数错误，修改失败", null);
		}
		Writing temp = writingService.display(id, isShow);
		if (null != temp) {
			return Results.success(null, temp);
		} else {
			return Results.fail(null, null);
		}
	}

}
