package com.wrt.sps.mgt.controller;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wrt.sps.entity.PageResults;
import com.wrt.sps.entity.Results;
import com.wrt.sps.mgt.entity.User;
import com.wrt.sps.mgt.pojo.UserDTO;
import com.wrt.sps.mgt.pojo.UserQueryDTO;
import com.wrt.sps.mgt.service.UserService;

/**
 * 用户管理接口
 * 
 * @author 文瑞涛
 * @date 2021年1月8日 上午10:44:36
 */
@Controller
@RequestMapping("/mgt/user")
public class UserController {

	@Resource
	private UserService userService;
	
	/**
	 * 
	 * 修改密码
	 * 
	 * @param 旧密码 oldpwd
	 * @param 新密码 newpwd
	 * 
	 * @return 修改密码结果
	 */
	@PostMapping("/changepwd")
	@ResponseBody
	public Results changepwd(String oldpwd, String newpwd) {
		if (StringUtils.isEmpty(oldpwd) || StringUtils.isEmpty(newpwd)) {
			return Results.fail("请输入新旧密码！", null);
		}
		User user = (User) SecurityUtils.getSubject().getPrincipal();
		boolean oldCorrect = user.judgePasswordCorrect(oldpwd);
		if (!oldCorrect) {
			return Results.fail("旧密码错误", null);
		}
		User oldUser = userService.findUserById(user.getId());
		oldUser.setPassword(newpwd);
		User newUser = userService.save(oldUser);
		return null != newUser ? Results.success("修改成功", null) : Results.fail("修改失败", null);
	}

	/**
	 * 
	 * 分页查询用户
	 * 
	 * @param 用户查询信息 dto
	 * @param 当前页码   page
	 * @param 每页显示条数 limit
	 * 
	 * @return 分页结果
	 */
	@GetMapping("/query")
	@ResponseBody
	public PageResults query(UserQueryDTO dto, @RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		return new PageResults(userService.query(dto, page - 1, limit));
	}

	/**
	 * 
	 * 保存用户
	 * 
	 * @param 用户数据   dto
	 * @param 校验错误结果 bindingResult
	 * 
	 * @return 保存结果
	 */
	@RequiresPermissions(value = { "user_add", "user_edit" }, logical = Logical.OR)
	@PostMapping("/save")
	@ResponseBody
	public Results save(@Validated UserDTO dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			FieldError fieldError = bindingResult.getFieldError();
			return Results.fail(null != fieldError ? fieldError.getDefaultMessage() : null, null);
		}
		if (dto.getLoginName().equals(User.ADMIN_LOGIN_NAME)) {
			return Results.fail("管理员不可修改", null);
		}
		User temp = userService.save(dto);
		if (null != temp) {
			return Results.success("用户信息保存成功", temp);
		} else {
			return Results.fail("用户信息保存失败", null);
		}
	}

	/**
	 * 
	 * 删除用户
	 * 
	 * @param 用户主键 id
	 * 
	 * @return 删除结果
	 */
	@RequiresPermissions("user_del")
	@DeleteMapping("/delete")
	@ResponseBody
	public Results delete(Long id) {
		if (null == id) {
			return Results.fail("参数错误，删除失败", null);
		}
		User temp = userService.findUserById(id);
		if (null != temp) {
			if (temp.getLoginName().equals(User.ADMIN_LOGIN_NAME)) {
				return Results.fail("管理员不可删除", null);
			}
			userService.delete(id);
			return Results.success(null, null);
		} else {
			return Results.fail("已删除", null);
		}
	}

}
