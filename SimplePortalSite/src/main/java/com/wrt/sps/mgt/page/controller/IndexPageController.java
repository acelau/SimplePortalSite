package com.wrt.sps.mgt.page.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mgt")
public class IndexPageController {

	/**
	 * 系统名称
	 */
	@Value("${system.name}")
	private String systemName;
	
	/**
	 * 系统版本
	 */
	@Value("${system.version}")
	private String systemVersion;

	/**
	 * 登录页
	 */
	@GetMapping("/index")
	public String index(Model model) {
		model.addAttribute("systemName", systemName);
		model.addAttribute("systemVersion", systemVersion);
		return "index";
	}
}
