package com.wrt.sps.mgt.listener;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.wrt.sps.mgt.entity.UploadFile;
import com.wrt.sps.mgt.event.UploadFileEvent;
import com.wrt.sps.mgt.service.UploadFileService;

/**
 * 上传文件事件监听器
 * 
 * @author 文瑞涛
 * @date 2021年4月11日 上午9:29:15
 */
@Component
public class UploadFileListener {
	
	@Resource
	private UploadFileService uploadFileService;

	/**
	 * 处理上传文件事件（持久化上传文件信息）
	 * 
	 * @param event 上传文件事件
	 */
	@EventListener
	public void handleUploadFileEvent(UploadFileEvent event) {
		UploadFile uf = event.getUploadFile();
		if(null != uf && StringUtils.isNotBlank(uf.getCode())) {
			uploadFileService.save(uf);
		}
	}
}
