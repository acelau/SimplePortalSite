package com.wrt.sps.mgt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wrt.sps.mgt.entity.Permission;

/**
 * 权限数据层
 * 
 * @author 文瑞涛
 * @date 2021年1月7日 上午9:53:44
 */
public interface PermissionDao extends JpaRepository<Permission, Long> {

	/**
	 * 
	 * 通过权限名称查找权限
	 * 
	 * @param pname 权限名称
	 * 
	 * @return 权限
	 */
	public Permission findPermissionByPermissionName(String pname);
}
