package com.wrt.sps.mgt.pojo;

import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 角色数据实体
 * 
 * @author 文瑞涛
 * @date 2020年12月19日 下午5:08:41
 */
@Data
public class RoleDTO {

	/**
	 * 角色主键
	 */
	private Long key;

	/**
	 * 角色名称
	 */
	@NotBlank(message = "角色名称不可为空")
	private String roleName;

	/**
	 * 角色权限数组
	 */
	private Long[] permissionIds;
}
