package com.wrt.sps.mgt.event;

import org.springframework.context.ApplicationEvent;

import com.wrt.sps.mgt.entity.UploadFile;

/**
 * 上传文件事件
 * 
 * @author 文瑞涛
 * @date 2021年4月11日 上午9:27:11
 */
public class UploadFileEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3235567197087407711L;
	
	private UploadFile uploadFile;

	public UploadFileEvent(Object source, UploadFile uploadFile) {
		super(source);
		this.uploadFile = uploadFile;
	}

	public UploadFile getUploadFile() {
		return this.uploadFile;
	}
}
