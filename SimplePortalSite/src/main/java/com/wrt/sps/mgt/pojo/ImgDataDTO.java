package com.wrt.sps.mgt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 图片信息实体类
 * 用于layui接收
 * 
 * @author 文瑞涛
 * 
 * @date 2021年4月12日 下午8:11:49
 */
@Data
@AllArgsConstructor
public class ImgDataDTO {

	/**
	 * 图片地址
	 */
	private String src;
	/**
	 * 图片标题
	 */
	private String title;
}
