package com.wrt.sps.mgt.pojo;

import lombok.Data;

/**
 * 用户查询数据实体类
 * 
 * @author 文瑞涛
 * @date 2020年12月20日 下午7:04:32
 */
@Data
public class UserQueryDTO {
	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 是否启用
	 */
	private Boolean validFlag;

}
