package com.wrt.sps.mgt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wrt.sps.mgt.entity.Role;

/**
 * 角色数据层
 * 
 * @author 文瑞涛
 * @date 2021年1月7日 上午9:53:12
 */
public interface RoleDao extends JpaRepository<Role, Long> {

	/**
	 * 
	 * 通过角色名称查询角色
	 * 
	 * @param roleName 角色名称
	 * 
	 * @return 角色
	 */
	Role findRoleByRoleName(String roleName);
}
