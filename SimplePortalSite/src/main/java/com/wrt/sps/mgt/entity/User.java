package com.wrt.sps.mgt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wrt.sps.entity.BaseEntity;
import com.wrt.sps.utils.MD5Util;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户实体类
 * 
 * @author 文瑞涛
 * @date 2020年11月30日 下午5:07:07
 */
@Entity
@Getter
@Setter
public class User extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7614695438024564644L;

	/**
	 * 默认密码
	 */
	private static final String DEFAULT_PASSWORD = "123456";

	/**
	 * 默认MD5加密混入字段
	 */
	public static final String SALT = "sps";

	/**
	 * 默认管理员登录名
	 */
	public static final String ADMIN_LOGIN_NAME = "admin";

	/**
	 * 登录名
	 */
	@Column(length = 100, nullable = false, unique = true)
	private String loginName;
	/**
	 * 密码
	 */
	@Column(length = 100, nullable = false)
	private String password;
	/**
	 * 是否启用
	 */
	@Column
	private Boolean validFlag;
	/**
	 * 上一次登录系统IP地址
	 */
	@Column
	private String lastLoginIP;
	/**
	 * 上一次登录系统时间
	 */
	@Column
	private Date lastLoginDate;
	/**
	 * 用户对应的角色
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	private Role role;

	/**
	 * 重写获取创建日期
	 */
	@Override
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getCreatedDate() {
		return super.getCreatedDate();
	}
	
	/**
	 * 重写获取上一次登录日期
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getLastLoginDate() {
		return this.lastLoginDate;
	}

	/**
	 * 
	 * 设置用户密码（加密）
	 * 
	 * @param 原始密码字符串 password
	 */
	public void setPassword(String password) {
		if (StringUtils.isBlank(password)) {
			this.password = MD5Util.encrypt(DEFAULT_PASSWORD, SALT);
		} else {
			this.password = MD5Util.encrypt(password, SALT);
		}
	}

	/**
	 * 创建管理员用户
	 */
	public void createAdmin() {
		this.setLoginName(ADMIN_LOGIN_NAME);
		this.setPassword(DEFAULT_PASSWORD);
		this.setValidFlag(true);
	}

	/**
	 * 
	 * 判断用户密码是否正确
	 * 
	 * @param 待判断的密码 password
	 * 
	 * @return 判断结果
	 */
	public boolean judgePasswordCorrect(String password) {
		boolean result;
		if (StringUtils.isBlank(password)) {
			result = false;
		} else {
			result = this.password.equalsIgnoreCase(MD5Util.encrypt(password, SALT));
		}
		return result;
	}
}
