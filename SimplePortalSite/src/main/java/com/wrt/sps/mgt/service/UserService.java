package com.wrt.sps.mgt.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.wrt.sps.mgt.entity.User;
import com.wrt.sps.mgt.pojo.UserDTO;
import com.wrt.sps.mgt.pojo.UserQueryDTO;

/**
 * 用户管理服务层
 * 
 * @author 文瑞涛
 * @date 2020年12月13日 下午2:42:48
 */
public interface UserService {

	/**
	 * 初始化管理员用户
	 */
	public void initAdminUser();

	/**
	 * 
	 * 通过登录名查询用户
	 * 
	 * @param loginName 登录名
	 * 
	 * @return 用户
	 */
	public User findUserByLoginName(String loginName);

	/**
	 * 
	 * 通过主键查询用户
	 * 
	 * @param id 用户主键
	 * 
	 * @return 用户
	 */
	public User findUserById(Long id);

	/**
	 * 
	 * 分页查询用户
	 * 
	 * @param dto 用户查询数据
	 * @param page 当前页码
	 * @param limit 每页显示条数
	 * 
	 * @return 分页查询结果
	 */
	public Page<User> query(UserQueryDTO dto, int page, int limit);
	
	/**
	 * 
	 * 统计当前系统内的用户数量
	 * 
	 * @return 数量集合
	 */
	public List<Long> countUsers();

	/**
	 * 
	 * 保存用户
	 * 
	 * @param dto 用户数据
	 * 
	 * @return 保存后的用户
	 */
	public User save(UserDTO dto);

	/**
	 * 
	 * 保存用户
	 * 
	 * @param user 用户
	 * 
	 * @return 保存后的用户
	 */
	public User save(User user);

	/**
	 * 
	 * 删除用户
	 * 
	 * @param id 用户主键
	 */
	public void delete(Long id);
}
