package com.wrt.sps.mgt.controller;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wrt.sps.entity.PageResults;
import com.wrt.sps.entity.Results;
import com.wrt.sps.mgt.entity.Role;
import com.wrt.sps.mgt.pojo.RoleDTO;
import com.wrt.sps.mgt.service.RoleService;

/**
 * 角色接口
 * 
 * @author 文瑞涛
 * @date 2021年1月9日 下午5:38:08
 */
@Controller
@RequestMapping("/mgt/role")
public class RoleController {

	@Resource
	private RoleService roleService;

	/**
	 * 
	 * 分页查询角色
	 * 
	 * @param 角色名称   rName
	 * @param 当前页码   page
	 * @param 每页显示条数 limit
	 * 
	 * @return 分页结果
	 */
	@GetMapping("/query")
	@ResponseBody
	public PageResults query(String rName, @RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit) {
		return new PageResults(roleService.query(rName, page - 1, limit));
	}

	

	/**
	 * 
	 * 保存角色
	 * 
	 * @param 角色数据   dto
	 * @param 校验错误结果 bindingResult
	 * 
	 * @return 保存结果
	 */
	@RequiresPermissions(value = { "role_add", "role_edit" }, logical = Logical.OR)
	@PostMapping("/save")
	@ResponseBody
	public Results save(@Validated RoleDTO dto, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			FieldError fieldError = bindingResult.getFieldError();
			return Results.fail(null != fieldError ? fieldError.getDefaultMessage() : null, null);
		}
		if (dto.getRoleName().equals(Role.ADMIN_ROLE_NAME)) {
			return Results.fail("管理员不可修改", null);
		}
		if(roleService.checkRoleExistByExample(dto.getRoleName())) {
			return Results.fail("角色名称已存在", null);
		}
		Role temp = roleService.saveRole(dto);
		if (null != temp) {
			return Results.success("角色信息保存成功", temp);
		} else {
			return Results.fail("角色信息保存失败", null);
		}
	}

	/**
	 * 
	 * 删除角色
	 * 
	 * @param 角色主键 id
	 * 
	 * @return 删除结果
	 */
	@RequiresPermissions("role_del")
	@DeleteMapping("/delete")
	@ResponseBody
	public Results delete(Long id) {
		if (null == id) {
			return Results.fail("参数错误，删除失败", null);
		}
		Role temp = roleService.findRoleById(id);
		if (null != temp) {
			if (temp.getRoleName().equals(Role.ADMIN_ROLE_NAME)) {
				return Results.fail("管理员不可删除", null);
			}
			roleService.delete(id);
			return Results.success(null, null);
		} else {
			return Results.fail("已删除", null);
		}
	}

}
