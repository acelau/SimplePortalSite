package com.wrt.sps.mgt.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.apache.commons.lang3.StringUtils;

import com.wrt.sps.entity.BaseEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * 权限实体类
 * 
 * @author 文瑞涛
 * @date 2021年1月6日 下午9:53:36
 */
@Entity
@Getter
@Setter
public class Permission extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4283709441950703526L;

	/**
	 * 权限组别
	 */
	public enum Group {
		/**
		 * 文章
		 */
		WRITING, 
		/**
		 * 用户
		 */
		USER, 
		/**
		 * 角色
		 */
		ROLE;
	}

	public Permission() {
	}

	/**
	 * 构造方法
	 * 
	 * @param 权限名称 name
	 */
	public Permission(String name) {
		super();
		if (StringUtils.isNotBlank(name)) {
			this.permissionName = name;
		}
	}

	/**
	 * 权限名称
	 */
	@Column(nullable = false, unique = true)
	private String permissionName;

	/**
	 * 
	 * 系统内所有的权限
	 * 
	 * @return 权限集合
	 */
	public static Set<Permission> allPermissions() {
		Set<Permission> allPermissionSet = new HashSet<>();
		allPermissionSet.add(new Permission("writing_add"));
		allPermissionSet.add(new Permission("writing_edit"));
		allPermissionSet.add(new Permission("writing_del"));
		allPermissionSet.add(new Permission("user_authority"));
		allPermissionSet.add(new Permission("user_add"));
		allPermissionSet.add(new Permission("user_edit"));
		allPermissionSet.add(new Permission("user_del"));
		allPermissionSet.add(new Permission("role_add"));
		allPermissionSet.add(new Permission("role_edit"));
		allPermissionSet.add(new Permission("role_del"));
		return allPermissionSet;
	}

	/**
	 * 
	 * 获取权限显示中文名称
	 * 
	 * @return 权限中文名称
	 */
	public String getDisplayName() {
		String result;
		switch (this.permissionName) {
		case "writing_add":
			result = "添加文章";
			break;
		case "writing_edit":
			result = "编辑文章";
			break;
		case "writing_del":
			result = "删除文章";
			break;
		case "user_authority":
			result = "权限设置";
			break;
		case "user_add":
			result = "添加用户";
			break;
		case "user_edit":
			result = "编辑用户";
			break;
		case "user_del":
			result = "删除用户";
			break;
		case "role_add":
			result = "添加角色";
			break;
		case "role_edit":
			result = "编辑角色";
			break;
		case "role_del":
			result = "删除角色";
			break;
		default:
			result = StringUtils.EMPTY;
			break;
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
