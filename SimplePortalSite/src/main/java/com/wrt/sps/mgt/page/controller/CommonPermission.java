package com.wrt.sps.mgt.page.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.ui.Model;

import com.wrt.sps.mgt.entity.Permission;

public class CommonPermission {

	/**
	 * 当前用户具备的权限
	 */
	protected void currentUserPermissions(Model model, Permission.Group group) {
		Subject subject = SecurityUtils.getSubject();
		boolean addPermission = false;
		boolean editPermission = false;
		boolean delPermission = false;
		switch (group) {
		case WRITING:
			addPermission = subject.isPermitted("writing_add");
			editPermission = subject.isPermitted("writing_edit");
			delPermission = subject.isPermitted("writing_del");
			break;
		case USER:
			addPermission = subject.isPermitted("user_add");
			editPermission = subject.isPermitted("user_edit");
			delPermission = subject.isPermitted("user_del");
			break;
		case ROLE:
			addPermission = subject.isPermitted("role_add");
			editPermission = subject.isPermitted("role_edit");
			delPermission = subject.isPermitted("role_del");
			break;
		default:
			break;
		}
		model.addAttribute("addPermission", addPermission);
		model.addAttribute("editPermission", editPermission);
		model.addAttribute("delPermission", delPermission);
	}
}
