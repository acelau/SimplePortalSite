package com.wrt.sps.mgt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.wrt.sps.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 上传文件实体类
 * 
 * @author 文瑞涛
 * @date 2021年4月10日 下午3:18:27
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class UploadFile extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2127317550187376037L;

	public enum Type{
		/**
		 * 图片
		 */
		IMG,
		/**
		 * 音乐
		 */
		MUSIC,
		/**
		 * 视频
		 */
		VIDEO,
		/**
		 * 办公文档
		 */
		OFFICE,
		/**
		 * 其他
		 */
		OTHERS
	}
	
	public UploadFile(String code, Boolean used) {
		this.used = used;
		this.code = code;
	}
	
	public UploadFile(String code, String name, String path, UploadFile.Type type) {
		this.used = false;
		this.code = code;
		this.name = name;
		this.path = path;
		this.type = type;
	}
	
	/**
	 * 文件编号
	 */
	@Column(nullable = false)
	private String code;
	/**
	 * 文件名称
	 */
	@Column(nullable = false)
	private String name;
	/**
	 * 文件地址
	 */
	@Column(nullable = false)
	private String path;
	/**
	 * 文件类型
	 */
	@Column(nullable = false)
	private UploadFile.Type type;
	/**
	 * 文件是否被使用
	 */
	@Column(nullable = false)
	private Boolean used;
}
