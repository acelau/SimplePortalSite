package com.wrt.sps.mgt.page.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wrt.sps.mgt.entity.Permission;

@Controller
@RequestMapping("/mgt")
public class AuthPageController extends CommonPermission {

	/**
	 * 用户角色
	 */
	@RequiresPermissions("user_authority")
	@GetMapping("/role")
	public String role(Model model) {
		currentUserPermissions(model, Permission.Group.ROLE);
		return "auth/role";
	}

	/**
	 * 用户管理
	 */
	@RequiresPermissions("user_authority")
	@GetMapping("/user")
	public String user(Model model) {
		currentUserPermissions(model, Permission.Group.USER);
		return "auth/user";
	}
}
