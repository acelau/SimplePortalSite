package com.wrt.sps.mgt.page.controller;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wrt.sps.mgt.service.RoleService;
import com.wrt.sps.mgt.service.UserService;

@Controller
@RequestMapping("/mgt/user")
public class UserPageController extends CommonPermission {

	@Resource
	private UserService userService;
	
	@Resource
	private RoleService roleService;
	
	/**
	 * 
	 * 添加用户（页面跳转）
	 * 
	 * @param model
	 * 
	 * @return 页面地址
	 */
	@RequiresPermissions("user_add")
	@GetMapping("/add")
	public String add(Model model) {
		commonHandle(model);
		model.addAttribute("titleName", "新增用户");
		return "auth/edit_user";
	}

	/**
	 * 
	 * 编辑用户（页面跳转）
	 * 
	 * @param 用户主键  id
	 * @param model
	 * 
	 * @return 页面地址
	 */
	@RequiresPermissions("user_edit")
	@GetMapping("/edit")
	public String edit(Long id, Model model) {
		if (null == id || id == 0) {
			return null;
		}
		commonHandle(model);
		model.addAttribute("user", userService.findUserById(id));
		model.addAttribute("titleName", "更新用户");
		return "auth/edit_user";
	}
	
	/**
	 * 
	 * 添加和编辑页面所需载入的系统角色
	 * 
	 * @param model
	 */
	private void commonHandle(Model model) {
		model.addAttribute("roles", roleService.getAllRoles());
	}
}
