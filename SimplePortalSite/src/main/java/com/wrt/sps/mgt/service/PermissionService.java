package com.wrt.sps.mgt.service;

import java.util.Set;

import com.wrt.sps.mgt.entity.Permission;

/**
 * 权限服务层
 * 
 * @author 文瑞涛
 * @date 2021年1月7日 上午11:37:15
 */
public interface PermissionService {
	/**
	 * 初始化系统权限
	 */
	public void initPermissions();

	/**
	 * 
	 * 获取系统内所有权限
	 * 
	 * @return 权限集合
	 */
	public Set<Permission> getAllPermissions();

	/**
	 * 
	 * 通过权限主键数组查询权限集合
	 * 
	 * @param ids 权限主键集合
	 * 
	 * @return 权限集合
	 */
	public Set<Permission> findPermissionsByIds(Long[] ids);
}
