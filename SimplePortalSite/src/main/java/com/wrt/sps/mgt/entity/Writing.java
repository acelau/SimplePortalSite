package com.wrt.sps.mgt.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wrt.sps.entity.BaseEntity;
import com.wrt.sps.mgt.pojo.WritingDTO;

import lombok.Getter;
import lombok.Setter;

/**
 * 文章实体类
 * 
 * @author 文瑞涛
 * @date 2020年11月30日 下午5:12:39
 */
@Entity
@Getter
@Setter
public class Writing extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4422817604997305920L;

	/**
	 * 文章类型
	 */
	public enum Type {
		/**
		 * 新闻
		 */
		NEWS,
		/**
		 * 服务
		 */
		SERVICE,
		/**
		 * 技术
		 */
		TECHNOLOGY
	}

	public Writing() {

	}

	/**
	 * 
	 * 构造方法
	 * 
	 * @param 文章主键 id
	 * @param 文章标题 title
	 * @param 文章简介 comments
	 * @param 图片地址 imgUrl
	 * @param 创建日期 createdDate
	 */
	public Writing(Long id, String title, String comments, String imgUrl, Date createdDate) {
		this.setId(id);
		this.setCreatedDate(createdDate);
		this.title = title;
		this.comments = comments;
		this.imgUrl = imgUrl;
	}

	/**
	 * 文章标题
	 */
	@Column(nullable = false)
	private String title;
	/**
	 * 文章简介
	 */
	@Column
	private String comments;
	/**
	 * 图片地址
	 */
	@Column
	private String imgUrl;
	/**
	 * 文章详情
	 */
	@Lob
	@Column(nullable = false)
	@Basic(fetch = FetchType.LAZY)
	private String details;
	/**
	 * 文章类型
	 */
	@Column(nullable = false)
	private Writing.Type type;
	/**
	 * 文章是否显示
	 */
	@Column
	private Boolean validFlag;

	/**
	 * 重写获取创建日期
	 * 
	 * @return 创建日期
	 */
	@Override
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getCreatedDate() {
		return super.getCreatedDate();
	}

	/**
	 * 获取类型名称
	 * 
	 * @param 文章类型 type
	 */
	public static String typeName(Type type) {
		String typeName = null;
		switch (type) {
		case NEWS:
			typeName = "新闻";
			break;
		case SERVICE:
			typeName = "服务";
			break;
		case TECHNOLOGY:
			typeName = "技术";
			break;
		default:
			break;
		}
		return typeName;
	}
	
	/**
	 * 获取类型名称(数组)
	 * 
	 * @param 文章类型 type
	 */
	public static String[] typeNames() {
		String [] typeNames = new String[Type.values().length];
		for(Type type:Type.values()) {
			typeNames[type.ordinal()] = typeName(type);
		}
		return typeNames;
	}
	
	public void setTitle(String title) {
		this.title = WritingDTO.safeString(title);
	}
	
	public void setComments(String comments) {
		this.comments = WritingDTO.safeString(comments);
	}
	
	public void setDetails(String details) {
		this.details = WritingDTO.safeString(details);
	}
}
