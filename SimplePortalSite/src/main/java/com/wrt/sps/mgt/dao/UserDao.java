package com.wrt.sps.mgt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wrt.sps.mgt.entity.Role;
import com.wrt.sps.mgt.entity.User;

/**
 * 用户数据层
 * 
 * @author 文瑞涛
 * @date 2020年11月30日 下午5:30:08
 */
public interface UserDao extends JpaRepository<User, Long> {

	/**
	 * 
	 * 通过登录名查询用户
	 * 
	 * @param loginName 用户登录名
	 *
	 * @return 用户
	 */
	public User findUserByLoginName(String loginName);

	/**
	 * 
	 * 通过角色查询用户
	 * 
	 * @param role 用户角色
	 * 
	 * @return 用户集合
	 */
	public List<User> findUserByRole(Role role);

}
