package com.wrt.sps.mgt.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import com.wrt.sps.mgt.entity.Writing;

/**
 * 文章数据层
 * 
 * @author 文瑞涛
 * @date 2020年11月30日 下午5:35:22
 */
public interface WritingDao extends CrudRepository<Writing, Long> {

	/**
	 * 
	 * 分页查询所有文章
	 * 
	 * @param spec 查询条件
	 * @param pageable 分页信息
	 * 
	 * @return 分页查询结果
	 */
	public Page<Writing> findAll(Specification<Writing> spec, Pageable pageable);

	/**
	 * 
	 * 通过文章是否显示标志进行计数
	 * 
	 * @param validFlag 是否显示
	 * 
	 * @return 数量
	 */
	public Long countByValidFlag(boolean validFlag);
	
	/**
	 * 
	 * 通过文章类型进行计数
	 * 
	 * @param type 文章类型
	 * 
	 * @return 数量
	 */
	public Long countByType(Writing.Type type);
}
