package com.wrt.sps.mgt.service;

import java.util.List;

import com.wrt.sps.mgt.entity.UploadFile;

/**
 * 上传文件服务层
 * 
 * @author 文瑞涛
 * @date 2021年4月10日 下午4:21:58
 */
public interface UploadFileService {

	/**
	 * 通过文件编号查询文件对象
	 * 
	 * @param code 文件编号
	 * 
	 * @return 上传文件对象
	 */
	public UploadFile findByCode(String code);
	
	/**
	 * 查询未被使用的上传文件
	 * 
	 * @return 未使用上传文件集合
	 */
	public List<UploadFile> findNotUsedFile();
	
	/**
	 * 保存上传文件信息
	 * 
	 * @param uploadFile 待存储的上传文件对象
	 * 
	 * @return 已存储的上传文件对象
	 */
	public UploadFile save(UploadFile uploadFile);
	
	/**
	 * 删除上传文件信息
	 * 
	 * @param id 上传文件信息主键
	 */
	public void delete(Long id);
}
